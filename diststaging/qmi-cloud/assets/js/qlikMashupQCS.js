var qlikMashup = (function() {
  
  const { qdtCapabilityApp, QdtViz } = QdtComponents;

  const initMyQdt = async function(config, returnto) {
  
    const urlLoggedIn = "/api/v1/users/me";//Use GET request to see if you are authenticated
    const urlLogin = "/login";
  
    const response = await fetch(`https://${config.host}${urlLoggedIn}`, {
        credentials: 'include',
        headers: {                  
            'Qlik-Web-Integration-ID': config.webIntegrationId
        }
    })

    if( response.status===401 ) {
      const url = new URL(`https://${config.host}${urlLogin}`);
      url.searchParams.append('returnto', returnto);
      url.searchParams.append('qlik-web-integration-id', config.webIntegrationId);
      window.location.href = url;
    }

    const user = await response.json();
    const capabilityApiAppPromise  = qdtCapabilityApp(config);
    const app = await capabilityApiAppPromise;
    
    return {app: app, user: user};

  };

  return {

    "initMyQdt": initMyQdt,

    "myQdtViz": function(app, divId, qlikObjectId, height, type){

      QdtViz({
        element: document.getElementById(divId),
        app,
        options: {
          type: type,
          id: qlikObjectId,
          height: height
        },
      });
    }

  }
  
})(qlikMashup||{})
  