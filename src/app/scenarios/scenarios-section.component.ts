import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-scenarios-section',
  templateUrl: './scenarios-section.component.html',
  styleUrls: ['./scenarios-section.component.scss']
})
export class ScenariosSectionComponent implements OnInit, OnDestroy {


  constructor(private router: Router, private _alertService: AlertService) { 
    
  }

  ngOnInit() {

    
  }

  ngOnDestroy() {}

  onStartProvision(scenario): void {
    
    //this.router.navigate(['provisions']);
    
    this._alertService.showAlert({
      type: 'alert-dark', 
      text: `Scenario <b>${scenario.name}</b> is going to be provisioned. Go to <a class='alert-link' href='/provisions'><b>My Provisions</b></a> to watch out the progress.`
    });
    
  }

}
