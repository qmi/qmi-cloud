import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { ProvisionsService } from '../services/provisions.service';

@Component({
  selector: 'cost-component',
  templateUrl: './cost.component.html',
  styleUrls: ['./cost.component.scss']
})
export class CostComponent implements OnInit,OnDestroy {

    subs : Subscription;
    qcsSheetUrl: any;
    constructor(private _provisionsService: ProvisionsService, private sanitizer: DomSanitizer) { }

    
    
    ngOnInit(): void {
        /*
        this.subs = this._provisionsService.getCurrentQCSUser().subscribe( value => {
            console.log("value", value);
            this.qcsSheetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(`${this._provisionsService.urlQlikServer}/single/?appid=1d2a43cf-8bc7-422c-90bd-d021cb232776&sheet=1ad3eb62-330d-45ac-8456-a6c8a76e044b&theme=breeze&opt=ctxmenu,currsel`);
        }, error => {            
            console.log('oops', error);
            const url = `${this._provisionsService.urlQlikServer}/login?qlik-web-integration-id=${this._provisionsService.webIntegrationId}&returnto=${window.location.href}`;
            window.location.href = url;
        });*/
  
    }

    ngOnDestroy(): void {
        //this.subs.unsubscribe();
    }

}
