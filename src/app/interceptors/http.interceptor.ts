import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthGuard } from '../services/auth.guard';


@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {

  constructor(private _auth: AuthGuard) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    var clonedRequest = req;
    if (sessionStorage.getItem('oktaAccessToken')  ) {
      clonedRequest =  req.clone({ setHeaders: {"oktaToken": sessionStorage.getItem('oktaAccessToken')} });
    }
    
    return next.handle(clonedRequest).pipe(tap(() => {},
      (err: any) => {

        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401) {
            return;
          }
          console.log("Interceptor error 401!!", err);
          if (err.status === 401 && err.url && err.url.indexOf("qlikcloud.com") !== -1) {
            return;
          }
          this._auth.clearUser();


          //this.router.navigate(['home']);
          window.location.href = "/login";

        } else {

        }
      }));
  }
}