import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthGuard } from '../services/auth.guard';
import { StatsService } from '../services/stats.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: []
})
export class HomeComponent implements OnInit {

  user;
  subs: Subscription;
  stats;
  vms;

  constructor( private _auth: AuthGuard, private _stats: StatsService ){
    this.subs = this._auth.getUserInfo().subscribe( value => {
      this.user = value;
    });
  }
  
  ngOnInit() {
    this._stats.getStats().subscribe(value => {
      this.stats = value;
    })

    this._stats.getVms().subscribe(value => {
      this.vms = value;
    })
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  popupConfirm(): void {
    console.log("Confirmed");
  }
  
}
