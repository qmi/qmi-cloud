import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {

  // Whether or not to enable provisions
  public disabledProvisions = false;

  constructor() {
  }

}
