import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class StatsService {

  constructor( private httpClient: HttpClient ) { }

  getStats() : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/stats`);
  }

  getVms() : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/stats/vms`);
  }

}
