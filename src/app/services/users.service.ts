import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';
import { concatMap, map, switchMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor( private httpClient: HttpClient ) { }

  getSessionInfo() : any {
    return this.httpClient.get(`/sessioninfo`);
  }

  getMe() {
    return this.httpClient.get(`${environment.apiVersionPath}/users/me`);
  }

  getUserById(userId) {
    return this.httpClient.get(`${environment.apiVersionPath}/users/${userId}`);
  }
  
  getUsers(sort: Boolean = false, filter: any = {}) : Observable<any> {
    
    let params = new HttpParams();
    params = params.append("filter", JSON.stringify(filter));

    return this.httpClient.get(`${environment.apiVersionPath}/users`, { params: params }).pipe(map((users:any)=>{
      if (sort){
        users.results = users.results.sort(function(a, b){return a.displayName.localeCompare(b.displayName);});
      }
      return users;
    }));
  }

  updateUser(userId, patchData): Observable<any> {
    return this.httpClient.put(`${environment.apiVersionPath}/users/${userId}`, patchData);
  }

  getNotifications(): Observable<any> {
    let params = new HttpParams();  
    params = params.append("page","1");
    params = params.append("size","300");
    
    return this.httpClient.get(`${environment.apiVersionPath}/notifications`,{ params: params });
  }

  getApiKeys(): Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/apikeys`);
  }

  getWebhooks(): Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/webhooks`);
  }

  addApikey(userId, body): Observable<any> {
    return this.httpClient.post(`${environment.apiVersionPath}/apikeys/${userId}`, body);
  }

  addWebhook(body): Observable<any> {
    return this.httpClient.post(`${environment.apiVersionPath}/webhooks`, body);
  }

  revokeApikey(id): Observable<any> {
    return this.httpClient.put(`${environment.apiVersionPath}/apikeys/${id}/revoke`, null);
  }

  delApikey(id): Observable<any> {
    return this.httpClient.delete(`${environment.apiVersionPath}/apikeys/${id}`);
  }

  delWebhook(id): Observable<any> {
    return this.httpClient.delete(`${environment.apiVersionPath}/webhooks/${id}`);
  }

  updateTitlePicture(userId): Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/users/${userId}/settitle`);
  }

  getScenarios(userId) : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/users/${userId}/scenarios`).pipe(map((scenarios:any)=>{
      scenarios.results.forEach(s => {
        s.description = decodeURI(s.description);
      });
      return scenarios;
    }));
  }
}
