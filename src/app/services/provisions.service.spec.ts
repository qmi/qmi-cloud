import { TestBed } from '@angular/core/testing';

import { ProvisionsService } from './provisions.service';

describe('ProvisionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProvisionsService = TestBed.get(ProvisionsService);
    expect(service).toBeTruthy();
  });
});
