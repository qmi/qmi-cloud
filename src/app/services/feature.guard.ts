import { Injectable } from '@angular/core';
import { CanActivate} from '@angular/router';

import { of, Observable, BehaviorSubject } from 'rxjs';

const _adminRoles = ['superadmin', 'admin'];

@Injectable({
  providedIn: 'root'
})
export class FeatureGuard implements CanActivate {

  private user: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor() {
    var user = sessionStorage.getItem("qmiuser");
    if (user){
      this.user.next(JSON.parse(user));
    } else {
      this.user.next(null);
    }
    
  }

  private _isAdmin(user) : boolean {
    return user && _adminRoles.includes(user.role);
  }

  canActivate(): Observable<boolean> {
    var user = this.user.value;
    console.log("CanActivate? #FeatureGuard", user);
    if ( this._isAdmin(user ) ) {
      return of(true);
    }
    if ( user && user.featureFlags){
      if (user.featureFlags.includes('training') ) {
        return of(true);
      } else {
        return of(false);
      }
      
    } else {
      of(false);
    }
    
  }

}


