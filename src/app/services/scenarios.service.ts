import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ScenariosService {

  constructor( private httpClient: HttpClient ) { }

  getScenarios() : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/scenarios`).pipe(map((scenarios:any)=>{
      scenarios.results.forEach(s => {
        s.description = decodeURI(s.description);
      });
      return scenarios;
    }));
  }

  getScenariosAll() : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/scenarios/all`).pipe(map((scenarios:any)=>{
      scenarios.results.forEach(s => {
        s.description = decodeURI(s.description);
      });
      return scenarios;
    }));
  }

  updateScenario(id, patchData) : Observable<any> {
    if (patchData.description){
      patchData.description = encodeURI(patchData.description);
    }
    return this.httpClient.put(`${environment.apiVersionPath}/scenarios/${id}`, patchData);
  }

  addScenario(data) : Observable<any> {
    if (data.description){
      data.description = encodeURI(data.description);
    }
    return this.httpClient.post(`${environment.apiVersionPath}/scenarios`, data);
  }

  deleteScenario(id) : Observable<any> {
    return this.httpClient.delete(`${environment.apiVersionPath}/scenarios/${id}`);
  }


  getScenarioVmtypes() : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/scenarios/vmtypes`);
  }

  createScenarioVmtype(data) : Observable<any> {
    return this.httpClient.post(`${environment.apiVersionPath}/scenarios/vmtypes`, data);
  }

  updateScenarioVmtype(id, data) : Observable<any> {
    return this.httpClient.put(`${environment.apiVersionPath}/scenarios/vmtypes/${id}`, data);
  }

  deleteScenarioVmtype(id) : Observable<any> {
    return this.httpClient.delete(`${environment.apiVersionPath}/scenarios/vmtypes/${id}`);
  }

}
