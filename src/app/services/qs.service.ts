import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QlikService {
  
  costSubject = new BehaviorSubject(null);
  
  all = {};


  private _formatMoney (num) {
    const dollars = new Intl.NumberFormat(`en-US`, {
      currency: `USD`,
      style: 'currency',
    }).format(num);
    return dollars;
  }

  private async _selectField(doc, field, value) {
    if (field && value) {
      const field1 = await doc.getField(field);
      await field1.selectValues([
          {
            qText: value,
          },
      ], false, true);
    }
  }

  private async _setCostData(doc, field = null, value = null) : Promise<any>{
    await this._selectField(doc, field, value);
    const properties = this._getProperties();
    const obj = await doc.createSessionObject(properties);
    const layout = await obj.getLayout();     
    layout.qHyperCube.qDataPages[0].qMatrix.forEach(m=>{
      this.all[m[0].qText.toLowerCase()] = {
        trigram: m[1].qText,
        active: m[2].qText,
        deleted: m[3].qText,
        amount: m[4].qNum, 
        dollars: this._formatMoney(m[4].qNum)
      };   
    });
    await doc.clearAll(); 
  }

  private _getProperties() : any {
    return {
      qInfo: {
        qType: 'my-straight-hypercube',
      },
      qHyperCubeDef: {
        qDimensions: [
          {
            qDef: { qFieldDefs: ['_id'] },
          },
          {
            qDef: { qFieldDefs: ['WD.Trigram'] },
          },
          {
            qDef: { qFieldDefs: ['isActive'] },
          },
          {
            qDef: { qFieldDefs: ['isDeleted'] },
          }
        ],
        qMeasures: [
          {
            qDef: { qDef: '=Sum(CostUSD)' },
          },
        ],
        qInitialDataFetch: [
          {
            qHeight: 2000,
            qWidth: 5,
          },
        ],
      },
    };
  }

  private async _getOpenApp(qsEmbed) : Promise<any> {
    const refApi = await qsEmbed.getRefApi();
    if ( !refApi ) {
      console.log("No refApi");
      return null;
    }
    const doc = await refApi.getDoc();
    if ( !doc ) {
      console.log("No Qlik doc");
      return null;
    }
    
    console.log("QS refApi and qsDoc opened!!");
    
    doc.session.on('closed', () => {
      console.log('Session was closed, clean up!');
    });
    
    doc.session.on('opened', () => {
      console.log('Session was opened!');
    });
    
    
    doc.session.on('suspended', () => {
      console.log('Session was suspended, retrying...');
      doc.session.resume();
    });
    
    doc.session.on('resumed', () => {
      console.log('Session was resumed, yay!');
    });

    await doc.clearAll();

    return doc;
  }

  async setCostData( qsEmbed, currentUser, trigram, provisionId ) {
    
    if ( !qsEmbed ) {
      console.log("No qsEmbed");
      return;
    }

    if ( provisionId ) {
      if ( !this.all[provisionId] ) {
        // Requesting data for a single provision
        const doc = await this._getOpenApp(qsEmbed);
        this._setCostData(doc, "_id", provisionId.toUpperCase());
      }
    } else if (trigram ) {
     
        // When Admin is requesting cost data for a User
        const doc = await this._getOpenApp(qsEmbed);
        this._setCostData(doc, "WD.Trigram", trigram);
    } else if (currentUser.role !== 'user') {
        // When Admin and we need to limit for isActive provisions
        const doc = await this._getOpenApp(qsEmbed);
        this._setCostData(doc, "isActive", "YES");  
    } else {
        const doc = await this._getOpenApp(qsEmbed);
        this._setCostData(doc);  
    }
    this.costSubject.next(this.all);
        
  }

}