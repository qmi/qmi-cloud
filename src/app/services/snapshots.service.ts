import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SnapshotsService {

  constructor( private httpClient: HttpClient ) { }

  getSnapshptsAll() : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/snapshots`);
  }

  updateSnapshot(id, patchData) : Observable<any> {
    return this.httpClient.put(`${environment.apiVersionPath}/snapshots/${id}`, patchData);
  }

  getSnapshot(id) : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/snapshots${id}`)
  }

  copySnapshotToRegions(id):Observable<any> {
    return this.httpClient.post(`${environment.apiVersionPath}/snapshots/${id}/copy`, null);
  }

  copySnapshotStatus(id):Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/snapshots/${id}/copystatus`);
  }

  getSnapshotLogs(id) : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/snapshots/${id}/logs`, {responseType: 'text'});
  }

}
