import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { of, Observable, BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UsersService } from './users.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  private userInfo: BehaviorSubject<any> = new BehaviorSubject(null);

  // Inject Router so we can hand off the user to the Login Page 
  constructor(private _userService: UsersService) {
    var user = sessionStorage.getItem("qmiuser");
    this.userInfo.next(JSON.parse(user));
  }

  canActivate(): Observable<boolean> {
    return this._userService.getSessionInfo().pipe(
      map( (session:any) => {
        if ( session.oktaAccessToken && session.user ) {
          sessionStorage.setItem("oktaAccessToken", session.oktaAccessToken);
          sessionStorage.setItem("qmiuser", JSON.stringify(session.user));
          this.userInfo.next(session.user);
          return true;
        } else {
          this.clearUser();
          return false;
        }
      }),
      catchError((err) => {
        
        return of(false);
      })
    );
  }  

  clearUser() {
    sessionStorage.removeItem("qmiuser");
    sessionStorage.removeItem("oktaAccessToken");
    this.userInfo.next(null);
  }

  getUserInfo(): any {
    return this.userInfo;
  }
}


