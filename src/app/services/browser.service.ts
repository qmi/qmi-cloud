import { Injectable } from '@angular/core';
import { BehaviorSubject, fromEvent } from 'rxjs';
 
@Injectable({
  providedIn: 'root',
})
export class BrowserService {
  private tabStatus = new BehaviorSubject<boolean>(true);
  public tabStatus$ = this.tabStatus.asObservable();
 
  public isTabActive(): boolean {
    return this.tabStatus.getValue();
  }
 
  constructor() {
    const visibilityChange$ = fromEvent(document, 'visibilitychange');
    visibilityChange$.subscribe(() => {
      if (document.visibilityState === 'visible') {
        this.tabStatus.next(true);
      } else {
        this.tabStatus.next(false);
      }
    });
  }
}