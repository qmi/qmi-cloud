import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class TrainingService {

  constructor( private httpClient: HttpClient ) { 
    
  }

  getTrainingSessionsAdmin(filter) : Observable<any> {
    let params = new HttpParams();
    params = params.append("filter", JSON.stringify(filter));
    return this.httpClient.get(`${environment.apiVersionPath}/training/sessions`, { params: params });
  }

  getTrainingSessions(userId) : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/training/${userId}/sessions`);
  }

  getTrainingSessionDetails(id) : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/training/session/${id}`);
  }

  getTemplates() : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/training/templates`);
  }


  addSession(userId, data): Observable<any> {
    return this.httpClient.post(`${environment.apiVersionPath}/training/${userId}/sessions`, data);
  }

  updateSession(userId, sessionId, patchData): Observable<any> {
    return this.httpClient.put(`${environment.apiVersionPath}/training/${userId}/sessions/${sessionId}`, patchData);
  }

  deleteSession(userId, sessionId): Observable<any> {
    return this.httpClient.delete(`${environment.apiVersionPath}/training/${userId}/sessions/${sessionId}`);
  }

  deleteSessionById(sessionId): Observable<any> {
    return this.httpClient.delete(`${environment.apiVersionPath}/training/session/${sessionId}`);
  }

  getStudents(id): Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/training/session/${id}/students`);
  }

  submit(id, data) : Observable<any> {
    return this.httpClient.post(`${environment.apiVersionPath}/training/session/${id}`, data);
  }
}
