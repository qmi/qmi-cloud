import { DOCUMENT } from '@angular/common';
import { Component, Inject } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'qmi-cloud';
  inFrame : boolean = false;
  header: boolean = false;

  constructor(@Inject(DOCUMENT) private document: any) {
    
    this.inFrame = (window.location !== window.parent.location);
    let pathname = this.document.location.pathname;
    this.header = pathname.indexOf("training/session/") === -1 && !this.inFrame;
      
  }
}
