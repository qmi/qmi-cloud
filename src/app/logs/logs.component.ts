import { Component, OnInit, Input, Output, EventEmitter, ViewChild, HostListener, OnChanges } from '@angular/core';
import { ProvisionsService } from '../services/provisions.service'; 
import { Subscription, timer} from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { BrowserService } from '../services/browser.service';
import { SnapshotsService } from '../services/snapshots.service';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent implements OnInit, OnChanges{

  @ViewChild("insideElement") insideElement;
  @HostListener('document:click', ['$event.target'])

  public onClick(targetElement) {
    const clickedInside = this.insideElement.nativeElement.contains(targetElement);
    if (!clickedInside) {
      console.log("Close Logs");
      this.onClose.emit(false);
    }
  }


  content: String = null;
  sub: Subscription;
  browserSubs: Subscription;

  constructor(private _provisionsService: ProvisionsService, private _snapshotsService: SnapshotsService, private _browserService: BrowserService) { }
  
  @Input() show;
  @Input() item;
  @Input() type;
  @Output() onClose = new EventEmitter();

  private _initTimerRequest(){
    console.log("log type", this.type, this.show);
    if ( this.type === "provision" ) {
      this.sub = timer(0, 7000).pipe( switchMap(() => this._provisionsService.getProvisionLogs(this.item._id) ) ).subscribe(content => { 
        this.content = content;
      });
    } else if ( this.type === "destroy" ) {
      this.sub = timer(0, 7000).pipe( switchMap(() => this._provisionsService.getDestroyLogs(this.item.destroy._id) ) ).subscribe(content => { 
        this.content = content;
      })
    } else {
      this.sub = timer(0, 7000).pipe( switchMap(() => this._snapshotsService.getSnapshotLogs(this.item._id) ) ).subscribe(content => { 
        this.content = content;
      })
    } 
  }

  ngOnInit() {
    this.browserSubs = this._browserService.tabStatus$.subscribe( active => {
      if (active && !this.sub && this.show){
        this._initTimerRequest();
      } else if (this.sub){
        console.log("Logs OFF");
        this.sub.unsubscribe();
        this.sub = null;
      }
    })
  }

  ngOnChanges(changes) {
    this.content = null;
    if ( this.sub ) {
      this.sub.unsubscribe();
      this.sub = null;
    }
    if ( changes.show && changes.show.currentValue ) {
      this._initTimerRequest();
    }
  }

  ngOnDestroy() {
    if (this.browserSubs ){
      this.browserSubs.unsubscribe();
    }
    if ( this.sub ) {
      this.sub.unsubscribe();
      this.sub = null;
    }
  }

  close(): void {
    this.content = null;
    if ( this.sub ) {
      this.sub.unsubscribe();
      this.sub = null;
    }
    this.onClose.emit(false);
  }

}
