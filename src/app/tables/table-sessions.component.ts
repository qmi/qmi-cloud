import { MdbTablePaginationComponent, MdbTableDirective, MDBModalService } from 'angular-bootstrap-md';

import { Component, OnInit, ViewChild, HostListener, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { UsersService } from '../services/users.service';
import { ApikeyModalComponent } from '../modals/edit-apikey.component';
import { ModalConfirmComponent } from '../modals/confirm.component';
import { TrainingService } from '../services/training.service';
import { SessionModalComponent } from '../training/edit-session.component';
import { SessionInfoModalComponent } from '../training/session-info.component';

@Component({
  selector: 'table-sessions',
  templateUrl: './table-sessions.component.html',
  styleUrls: ['./table-sessions.component.scss']
})
export class TableSessionsComponent implements OnInit, AfterViewInit {
  
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  
  previous: any = [];
  searchText: string = '';
  maxVisibleItems: number = 25;

  loading: boolean = false;
  elements = [];
  filter = {
    active: false
  }

  @HostListener('input') oninput() {
    this.mdbTablePagination.searchText = this.searchText;
  }

  constructor(private modalService: MDBModalService, private cdRef: ChangeDetectorRef, private _trainingService: TrainingService) {  
  }

  private _initElements(): void {
    this.mdbTable.setDataSource(this.elements);
    this.elements = this.mdbTable.getDataSource();
    this.previous = this.mdbTable.getDataSource();
  }

  ngOnInit() {
    this.refreshData();
  }

  refreshData() {
    this.loading = true;
    this.searchText = "";
    let actualFilter = {};
    if ( !this.filter.active ) {
      actualFilter["status"] = {"$ne": "terminated"};
    }
    var sub = this._trainingService.getTrainingSessionsAdmin(actualFilter).subscribe( res => {
        sub.unsubscribe();
        this.elements = res.results; 
        this.loading = false;
        this._initElements();
    });
  }

  ngAfterViewInit() {
    
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();

    this.mdbTable.searchDataObservable(this.searchText).subscribe(() => {
      this.mdbTablePagination.calculateFirstItemIndex();
      this.mdbTablePagination.calculateLastItemIndex();
    });
  }

  editSession(session) {
    var modalRef = this.modalService.show(SessionModalComponent, {
      class: 'modal-lg',
      containerClass: '',
      data: {
        template: session.template,
        session: session,
        user: session.user
      }
    } );
    
    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
      this.refreshData();
    });
  }

  infoSession(session) {
    var modalRef = this.modalService.show(SessionInfoModalComponent, {
      class: 'modal-md',
      containerClass: '',
      data: {
        session: session
        
      }
    } );
    
    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
    });
  }

  openConfirmDeleteModal(session) {
    var modalRef = this.modalService.show(ModalConfirmComponent, {
      class: 'modal-sm modal-notify modal-danger',
      containerClass: '',
      data: {
        info: {
          title: 'Confirm delete?',
          icon: 'trash-alt'
        }
      }
    } );

    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
      this._trainingService.deleteSessionById(session._id).subscribe( res=> {
        this.refreshData();
      });      
    });
  }

  onCheckValue() : void {
    this.refreshData();
  }
  

}