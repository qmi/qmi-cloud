import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopoverconfirmComponent } from './popoverconfirm.component';

describe('PopoverconfirmComponent', () => {
  let component: PopoverconfirmComponent;
  let fixture: ComponentFixture<PopoverconfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopoverconfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopoverconfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
