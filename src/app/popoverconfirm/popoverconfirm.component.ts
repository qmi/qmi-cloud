import { Component, OnInit, ElementRef, EventEmitter, ViewChild, Input, HostListener, Output } from '@angular/core';

@Component({
  selector: 'app-popoverconfirm',
  templateUrl: './popoverconfirm.component.html',
  styleUrls: ['./popoverconfirm.component.scss']
})
export class PopoverconfirmComponent implements OnInit {

  private element;

  @ViewChild('popovercontent')
  private popovercontent : ElementRef; 
  
  constructor( myElement: ElementRef ) { 
    this.element = myElement;
  }

  @Input() dock;
  @Input() buttonConfig;
  @Input() popupConfig;

  @Output() onConfirm = new EventEmitter();

  top;
  left;

  open(): void {
    let viewportOffset = this.element.nativeElement.getBoundingClientRect();
    let top = viewportOffset.top;
    let left = viewportOffset.left;
    
    this.popovercontent.nativeElement.style.display = 'block';
    this.popovercontent.nativeElement.style.position = 'absolute';

    if (this.dock.indexOf('left') !== -1) {
      this.left = -this.popovercontent.nativeElement.offsetWidth;
    } else if ( this.dock.indexOf('right') !== -1 ) {
      this.left = this.element.nativeElement.offsetWidth;
    }

    this.top = 0;
    if (this.dock.indexOf('top') !== -1) {
      this.top = -this.popovercontent.nativeElement.offsetHeight;
    } else if (this.dock.indexOf('bottom') !== -1) {
      this.top = this.element.nativeElement.offsetHeight;
    }

    this.popovercontent.nativeElement.style.top = (top + this.top) + 'px';
    this.popovercontent.nativeElement.style.left = (left + this.left) + 'px';
  
  }

  ngOnInit() {
  }

  ok(): void {
    this.popovercontent.nativeElement.style.display = 'none';
    this.onConfirm.emit(true);
  }

  cancel(): void {
    this.popovercontent.nativeElement.style.display = 'none';
  }
}
