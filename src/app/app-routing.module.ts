import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProvisionsComponent }   from './provisions/provisions.component';
import { AdminComponent }   from './admin/admin.component';
import { StatsComponent }   from './stats/stats.component';
import { HomeComponent }   from './home/home.component';
import { AuthGuard } from './services/auth.guard';
import { FaqComponent } from './faq/faq.component';
import { UserDashboardComponent } from './user/user-dashboard.component';
import { ScenariosSectionComponent } from './scenarios/scenarios-section.component';
import { ProvisionsSharedComponent } from './provisions/provisions-shared.component';
import { CostComponent } from './cost/cost.component';
import { ProvComponent } from './provisions/prov.component';
import { TrainingComponent } from './training/training.component';
import { SessionFormComponent } from './sessionform/sessionform.component';
import { FeatureGuard } from './services/feature.guard';
import { ErrorComponent } from './home/error.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'faq', component: FaqComponent},
  { path: 'error', component: ErrorComponent},
  { path: 'scenarios', component: ScenariosSectionComponent, canActivate: [AuthGuard]},
  { path: 'provision/:id', component: ProvComponent, canActivate: [AuthGuard]},
  { path: 'snapshots/:id', component: ProvComponent, canActivate: [AuthGuard]},
  { path: 'provisions', component: ProvisionsComponent, canActivate: [AuthGuard]},
  { path: 'sharedprovision', component: ProvisionsSharedComponent, canActivate: [AuthGuard]},
  { path: 'cost-analysis', component: CostComponent, canActivate: [AuthGuard]},
  { path: 'admin', component: AdminComponent, canActivate: [AuthGuard]},
  { path: 'admin/:tab', component: AdminComponent, canActivate: [AuthGuard]},
  { path: 'stats', component: StatsComponent, canActivate: [AuthGuard]},
  { path: 'training', component: TrainingComponent, canActivate: [AuthGuard, FeatureGuard]},
  { path: 'training/session/:id/public', component: SessionFormComponent},
  { path: 'user/:id', component: UserDashboardComponent, canActivate: [AuthGuard]},
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: '**', redirectTo: '/home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
