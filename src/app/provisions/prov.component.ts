import { Component, OnInit } from '@angular/core';
import { ProvisionsService } from '../services/provisions.service'; 
import { AuthGuard } from '../services/auth.guard';
import { ActivatedRoute } from '@angular/router';
import { MDBModalService } from 'angular-bootstrap-md';
import { AlertService } from '../services/alert.service';
import { Subscription } from 'rxjs';
import { ModalConfirmComponent } from '../modals/confirm.component';
import { QlikService } from '../services/qs.service';
import { SnapshotsService } from '../services/snapshots.service';


@Component({
  selector: 'app-prov',
  templateUrl: './prov.component.html',
  styleUrls: ['./prov.component.scss'],
  providers: [ProvisionsService]
})
export class ProvComponent implements OnInit {

   subscription: Subscription;
   provisionId;
   provision;
   logItem;
   creating: boolean = false;
   events;
   snapshots;
   private _userId;
   userRole;
   logstype: String = 'provision';
   logShow: boolean = false;
   sharedUsers;
   acl = { "type": "view"};
   costData;
   currentRout: String = null;
   isSnapshots: boolean = false;
s
  constructor( private modalService: MDBModalService, private _snapshotsService: SnapshotsService, private _alertService: AlertService, private _provisionsService: ProvisionsService, private _auth: AuthGuard, private route: ActivatedRoute, private _qlikService: QlikService ) {

    this._auth.getUserInfo().subscribe( value => {
      this._userId = value? value._id : null;
      this.userRole = value? value.role : null;
    });
    this.route.params.subscribe(params => {
        this.provisionId = params['id']; // (+) converts string 'id' to a number
    });

    this.currentRout = window.location.href;

    this.isSnapshots = this.currentRout.includes("snapshot");
  }

  private _refreshEvents() {
    var es = this._provisionsService.getEvents(this.provision.user._id, this.provisionId).subscribe(res=> {
        this.events = res.results;
        es.unsubscribe();
      });
  }

  private _refreshSnapshots() {
    var es = this._provisionsService.getSnapshots(this.provisionId).subscribe(res=> {
        this.snapshots = res.results;
        es.unsubscribe();
      });
  }
  
  private _refreshShares() {
    var ss = this._provisionsService.getSharesForProvision(this.provision.user._id, this.provisionId).subscribe(res=> {
        this.sharedUsers = res;  
        ss.unsubscribe(); 
    });
  }

  ngOnInit() {

    this.subscription = this._provisionsService.getProvisionUserById(this._userId, this.provisionId).subscribe(provision => { 
        this.provision = provision;
        this._refreshEvents();
        this._refreshShares();
        this._refreshSnapshots();
    });

    this._qlikService.costSubject.subscribe(function(value){
      this.costData = value;
    }.bind(this));

  }

  createNewSnap(): void {
    this.creating = true;
    var instantSubs = this._provisionsService.createSnapshots(this.provision._id).subscribe( provisions=>{
      instantSubs.unsubscribe();
      this._refreshSnapshots();
      this.creating = false;
    });
  }


  copySnap(snap): void {
    var instantSubs = this._snapshotsService.copySnapshotToRegions(snap._id).subscribe( result =>{
      instantSubs.unsubscribe();
      this._refreshSnapshots();
      this._alertService.showAlert({
        type: 'alert-dark', 
        text: `Snapshot is now being distributed to regions.`
      });
    });
  }

  checkCopySnap(snap): void {
    var instantSubs = this._snapshotsService.copySnapshotStatus(snap._id).subscribe( result =>{
      instantSubs.unsubscribe();
      this._refreshSnapshots();
      this._alertService.showAlert({
        type: 'alert-dark', 
        text: `Checking copy status.... Please show Logs to display result.`
      });
    });
  }

  getCost(id) : string {

    if (this.costData && this.costData[id] ) {
      return this.costData[id].dollars;
    }
    return "n/a";
  }


  ngOnDestroy() { 
    if (this.subscription){
        this.subscription.unsubscribe();
    }
  }

  getFlag(provision) : string {
    var flag = "us";
    if (provision.deployOpts.location === "West Europe") {
      flag = "ie";
    } else if (provision.deployOpts.location === "Southeast Asia") {
      flag = "sg";
    }
    return flag;
  }

  getOptionName(key){
    if ( this.provision && this.provision._scenarioDoc && this.provision._scenarioDoc.availableProductVersions){
      var found = this.provision._scenarioDoc.availableProductVersions.find(function(opt) {
        return opt.index === key
      });
      if (found){
        return found.product? found.product : found.index;
      } else {
        return key;
      }
    } else {
      return key;
    }
  }

  onLogsClose(): void {
    this.logShow = false;
  }

  showLogs($event, type): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.logItem = this.provision;
    this.logstype = type;
    this.logShow = true;
  }

  showSnapshotLogs($event, snap, type): void {
    console.log("SHOW snap logs", snap);
    $event.preventDefault();
    $event.stopPropagation();
    this.logItem = snap;
    this.logstype = type;
    this.logShow = true; 
  }


  onImgError(event, user){
    event.target.src = 'https://ui-avatars.com/api/?name='+user.displayName+'&size=40&background=00807b&color=fff'
  }

  openModalChangeSuccess(provision) {
    var modalRef = this.modalService.show(ModalConfirmComponent, {
      class: 'modal-sm modal-notify modal-danger',
      containerClass: '',
      data: {
        info: {
          title: 'Confirm change to a successful provision?',
          icon: 'tick'
        }
      }
    } );

    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
      this._provisionsService.updateProvisionUser(provision._id.toString(), this._userId, {"status": "provisioned"}).subscribe( res => {
        provision.status = res.provision.status;
        this._alertService.showAlert({
          type: 'alert-dark', 
          text: `Provision status changed to  <b>'provisioned'</b>.`
        });
      });
    });
  }


}