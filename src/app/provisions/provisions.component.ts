import { Component, OnInit,OnDestroy } from '@angular/core';
import { ProvisionsService } from '../services/provisions.service'; 
import { Subscription, timer} from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthGuard } from '../services/auth.guard';
import { AlertService } from '../services/alert.service';
import { MDBModalService } from 'angular-bootstrap-md';
import { ModalInfoComponent } from '../modals/modalinfo.component';
import { ModalConfirmComponent } from '../modals/confirm.component';
import { ProvisionModalComponent } from '../modals/edit-provision.component';
import { ShareModalComponent } from '../modals/share.component';
import { QlikService } from '../services/qs.service';
import { BrowserService } from '../services/browser.service';


@Component({
  selector: 'app-provisions',
  templateUrl: './provisions.component.html',
  styleUrls: ['./provisions.component.scss'],
  providers: [ProvisionsService]
})
export class ProvisionsComponent implements OnInit, OnDestroy {

  private _userId;
  provisions;
  destroys;
  subscription: Subscription;
  browserSubs: Subscription;
  instantSubs: Subscription;
  subCost: Subscription;
  logShow: boolean = false;
  logstype: String = 'provision';
  public selectedprov: Object = null;
  history: Boolean = false;
  costData: any = null;
  currentUser;

  
  constructor(private modalService: MDBModalService, private _alertService: AlertService, private _provisionsService: ProvisionsService, private _auth: AuthGuard, private _qlikService: QlikService, private _browserService: BrowserService) {
    this._auth.getUserInfo().subscribe( value => {
      this._userId = value? value._id : null;
      this.currentUser = value;
    });

    this.browserSubs = this._browserService.tabStatus$.subscribe( active => {
      if (active && !this.subscription){
        this._initTimerRequest();
      } else if (this.subscription){
        this.subscription.unsubscribe();
        this.subscription = null;
      }
    })
  }

  

  private _refresh(): void {
    this.instantSubs = this._provisionsService.getProvisionsByUser(this._userId).subscribe( provisions=>{
      provisions = provisions.results;
      this.provisions = provisions.filter(p => !p.isDestroyed && (!p.destroy || !p.destroy.status || p.destroy.status !== 'destroyed'));
      this.destroys = provisions.filter(p => p.isDestroyed || (p.destroy && p.destroy.status === 'destroyed'));
      this.instantSubs.unsubscribe();
    })
  }

  private _initTimerRequest() {
    this.subscription = timer(0, 8000).pipe( switchMap(() => this._provisionsService.getProvisionsByUser(this._userId) ) ).subscribe(provisions => { 
      provisions = provisions.results;
      this.provisions = provisions.filter(p => !p.isDestroyed && (!p.destroy || !p.destroy.status || p.destroy.status !== 'destroyed'));
      this.destroys = provisions.filter(p => p.isDestroyed || (p.destroy && p.destroy.status === 'destroyed'));
      if ( this.provisions && this.provisions.length === 0 ) {
        this.subscription.unsubscribe();
        this.subscription = null;
      }
    });
  }

  ngOnInit() {    
    
    this.subCost = this._qlikService.costSubject.subscribe(function(value){
      this.costData = value;
    }.bind(this));
    
  }

  getFlag(provision) : string {
    var flag = "us";
    if (provision.deployOpts.location === "West Europe") {
      flag = "ie";
    } else if (provision.deployOpts.location === "Southeast Asia") {
      flag = "sg";
    }
    return flag;
  }

  ngOnDestroy() {
    this.browserSubs.unsubscribe();
    if (this.subscription){
      this.subscription.unsubscribe();
      this.subscription = null;
    }
    if ( this.instantSubs ) {
      this.instantSubs.unsubscribe();
    }

    if (this.subCost){
      this.subCost.unsubscribe();
    }
  }

  setModal(provision, frame) : void {
    frame.show();
    this._provisionsService.setSelectedProv(provision);
  }

  getCost(id){
    return this.costData && this.costData[id]? this.costData[id].dollars : "n/a";
  }


  del(provision): void {
    this._provisionsService.delProvision(provision._id.toString(), this._userId).subscribe( res => {
      this._refresh();
      this._alertService.showAlert({
        type: 'alert-dark', 
        text: `Provision entry <b>${provision.scenario}</b> was deleted from your history`
      });
    })
  }

  toggleHistory(): void {
    this.history = !this.history;
  }

  openConfirmDestroyModal(provision) {
    var modalRef = this.modalService.show(ModalConfirmComponent, {
      class: 'modal-sm modal-notify modal-danger',
      containerClass: '',
      data: {
        info: {
          title: 'Confirm destroy this provision?',
          icon: 'times-circle'
        }
      }
    } );

    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
      this._provisionsService.newDestroy(provision._id.toString(), this._userId).subscribe( res => {
        this._refresh();
        this._alertService.showAlert({
          type: 'alert-dark', 
          text: `Provision of scenario <b>${provision.scenario}</b> is going to be destroyed.`
        });
      });
    });
  }

  openRemoteAccess(provision) {
    let url = provision.outputs.WEB_RDP_ACCESS_WITH_GUACAMOLE || provision.outputs.WEB_SSH_ACCESS_WITH_GUACAMOLE;
    window.open(url, provision._id);
  }

  openConfirmStopModal(provision) {
    var modalRef = this.modalService.show(ModalConfirmComponent, {
      class: 'modal-sm modal-notify modal-info',
      containerClass: '',
      data: {
        info: {
          title: 'Confirm Stop?',
          icon: 'stop',
          buttonColor: 'grey'
        },
        provision: provision
      }
    } );

    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
      this._provisionsService.stopVms(provision._id.toString(), this._userId, result).subscribe( res => {
        provision.statusVms = res.statusVms;
        provision.timeRunning = res.timeRunning;
        provision.runningFrom = res.runningFrom;
        this._alertService.showAlert({
          type: 'alert-dark', 
          text: `Stopping scenario <b>${provision.scenario}</b>...`
        });
      })
    });
  }

  share(provision) {
    var modalRef = this.modalService.show(ShareModalComponent, {
      class: 'modal-md modal-notify',
      containerClass: '',
      data: {
        provision: provision
      }
    } );
    
    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
      
    });
  }

  openConfirmStartModal(provision) {
    var modalRef = this.modalService.show(ModalConfirmComponent, {
      class: 'modal-sm modal-notify modal-info',
      containerClass: '',
      data: {
        info: {
          title: 'Confirm Start?',
          icon: 'play',
          buttonColor: 'grey'
        },
        provision: provision
      }
    } );

    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
      this._provisionsService.startVms(provision._id.toString(), this._userId).subscribe( res => {
        provision.statusVms = res.statusVms;
        provision.timeRunning = res.timeRunning;
        provision.runningFrom = res.runningFrom;
        this._alertService.showAlert({
          type: 'alert-dark', 
          text: `Starting scenario <b>${provision.scenario}</b>...`
        });
      })
    });
  }


  openConfirmExtendModal($event, provision) {
    $event.preventDefault();
    var modalRef = this.modalService.show(ModalConfirmComponent, {
      class: 'modal-sm modal-notify modal-info',
      containerClass: '',
      data: {
        info: {
          title: `Renew 24x7 period?`,
          icon: 'redo-alt',
          buttonColor: 'grey'
        }
      }
    } );

    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
      this._provisionsService.extend(provision._id.toString(), this._userId).subscribe( res => {
        provision.countExtend = res.countExtend;
        provision.timeRunning = res.timeRunning;
        provision.runningFrom = res.runningFrom;
        this._alertService.showAlert({
          type: 'alert-dark', 
          text: `Running period extended another ${provision._scenarioDoc.allowed24x7RunningDays} days (from now) for provision <b>${provision.scenario}</b>`
        });
      });
    });
  }

  openScheduleModal(provision) {
    var modalRef = this.modalService.show(ProvisionModalComponent, {
      class: 'modal-lg modal-notify',
      containerClass: '',
      data: {
        provision: provision
      }
    } );

    var sub = modalRef.content.action.subscribe( (data: any) => { 
      sub.unsubscribe();
      
    });
  }

  showLogs($event, provision, type): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.logstype = type;
    this.logShow = false;
    this.selectedprov = provision;
    this.logShow = true;
  }

  openModal(provision) {
    this.modalService.show(ModalInfoComponent, {
      class: 'modal-lg',
      containerClass: '',
      data: {
        provisionId:provision._id
      }
    } );
  }

  onLogsClose(): void {
    this.selectedprov = null;
    this.logShow = false;
  }

  onStartProvision(scenario): void {
    this._alertService.showAlert({
      type: 'alert-dark', 
      text: `Scenario <b>${scenario.name}</b> is going to be provisioned. Scroll up to your Provisions to watch out progress.`
    });
    this._refresh();
  }

}
