import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProvisionsService } from '../services/provisions.service'; 
import { UsersService } from '../services/users.service';
import { MDBModalService } from 'angular-bootstrap-md';
import { ModalInfoComponent } from '../modals/modalinfo.component';
import { ProvisionModalComponent } from '../modals/edit-provision.component';
import { ShareModalComponent } from '../modals/share.component';
import { ModalConfirmComponent } from '../modals/confirm.component';
import { QlikService } from '../services/qs.service';
import { UserModalComponent } from '../modals/edit-user.component';
import { AuthGuard } from '../services/auth.guard';
import { AlertService } from '../services/alert.service';


@Component({
  selector: 'user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit, OnDestroy{

    id: string;
    provisions;
    destroys;
    user;
    private sub: any;
    private instantSubs: any;
    public selectedprov: Object = null;
    history: Boolean = false;
    currentUser;
    costData;
    logShow: boolean = false;
    logstype: String = 'provision';

    constructor(private modalService: MDBModalService,private _alertService: AlertService, private route: ActivatedRoute, private _provisionsService: ProvisionsService, private _userService: UsersService, private _qlikService: QlikService, private _auth: AuthGuard) { 

      this._auth.getUserInfo().subscribe( value => {
        this.currentUser = value;
      });  
    }
    
    
    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.id = params['id']; // (+) converts string 'id' to a number
     

            this.instantSubs = this._provisionsService.getProvisionsByUser(this.id).subscribe( provisions=>{
                provisions = provisions.results;
                this.provisions = provisions.filter(p => !p.isDestroyed && (!p.destroy || !p.destroy.status || p.destroy.status !== 'destroyed'));
                this.destroys = provisions.filter(p => p.isDestroyed || (p.destroy && p.destroy.status === 'destroyed'));
                this.instantSubs.unsubscribe();
            });

            this._userService.getUserById(this.id).subscribe( user=> {
                this.user = user;
            })
        });
        this._qlikService.costSubject.subscribe(function(value){
          this.costData = value;
        }.bind(this));
    
    }

    isCurrentUserOrAdmin() : boolean {
      return this.currentUser && ( this.currentUser._id.toString() === this.id || this.currentUser.role === 'admin' || this.currentUser.role === 'superadmin');
    }
    
    getCost(id) : string {
  
      if (this.costData && this.costData[id] ) {
        return this.costData[id].dollars;
      }
      return "n/a";
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    openConfirmStartModal(provision) {
      var modalRef = this.modalService.show(ModalConfirmComponent, {
        class: 'modal-sm modal-notify modal-info',
        containerClass: '',
        data: {
          info: {
            title: 'Confirm Start?',
            icon: 'play',
            buttonColor: 'grey'
          },
          provision: provision
        }
      } );
  
      var sub = modalRef.content.action.subscribe( (result: any) => { 
        sub.unsubscribe();
        this._provisionsService.startVms(provision._id.toString(),  this.currentUser._id.toString()).subscribe( res => {
          provision.statusVms = res.statusVms;
          provision.timeRunning = res.timeRunning;
          provision.runningFrom = res.runningFrom;
          this._alertService.showAlert({
            type: 'alert-dark', 
            text: `Starting scenario <b>${provision.scenario}</b>...`
          });
        })
      });
    }

    openConfirmStopModal(provision) {
      var modalRef = this.modalService.show(ModalConfirmComponent, {
        class: 'modal-sm modal-notify modal-info',
        containerClass: '',
        data: {
          info: {
            title: 'Confirm Stop?',
            icon: 'stop',
            buttonColor: 'grey'
          },
          provision: provision
        }
      } );
  
      var sub = modalRef.content.action.subscribe( (result: any) => { 
        sub.unsubscribe();
        this._provisionsService.stopVms(provision._id.toString(),  this.currentUser._id.toString(), result).subscribe( res => {
          provision.statusVms = res.statusVms;
          provision.timeRunning = res.timeRunning;
          provision.runningFrom = res.runningFrom;
          this._alertService.showAlert({
            type: 'alert-dark', 
            text: `Stopping scenario <b>${provision.scenario}</b>...`
          });
        })
      });
    }

    showLogs($event, provision, type): void {
        $event.preventDefault();
        $event.stopPropagation();
        this.logstype = type;
        this.logShow = false;
        this.selectedprov = provision;
        this.logShow = true;
    }

    toggleHistory(): void {
      this.history = !this.history;
    }

    onLogsClose(): void {
        this.selectedprov = null;
        this.logShow = false;
    }

    editUser(user) {
      var modalRef = this.modalService.show(UserModalComponent, {
        class: 'modal-lg',
        containerClass: '',
        data: {
          userId: user._id,
          currentUser: this.currentUser
        }
      } );
      
      var sub = modalRef.content.action.subscribe( (result: any) => { 
        user = result;
        sub.unsubscribe();
      });
    }
    

    onImgError(event, user){
        event.target.src = 'https://ui-avatars.com/api/?name='+user.displayName+'&size=40&background=00807b&color=fff'
    }

    getFlag(provision) : string {
      var flag = "us";
      if (provision.deployOpts.location === "West Europe") {
        flag = "ie";
      } else if (provision.deployOpts.location === "Southeast Asia") {
        flag = "sg";
      }
      return flag;
    }

    share(provision) {
      var modalRef = this.modalService.show(ShareModalComponent, {
        class: 'modal-md modal-notify',
        containerClass: '',
        data: {
          provision: provision
        }
      } );
      
      var sub = modalRef.content.action.subscribe( (result: any) => { 
        sub.unsubscribe();
        
      });
    }

    openModal(provision) {
      this.modalService.show(ModalInfoComponent, {
        class: 'modal-lg',
        containerClass: '',
        data: {
          provisionId: provision._id
        }
      } );
    }

    openScheduleModal(provision) {
        var modalRef = this.modalService.show(ProvisionModalComponent, {
          class: 'modal-lg modal-notify',
          containerClass: '',
          data: {
            provision: provision
          }
        } );
    
        var sub = modalRef.content.action.subscribe( (data: any) => { 
          sub.unsubscribe();
          
        });
    }

    openConfirmDestroyModal(provision) {
      var modalRef = this.modalService.show(ModalConfirmComponent, {
        class: 'modal-sm modal-notify modal-danger',
        containerClass: '',
        data: {
          info: {
            title: 'Confirm destroy this provision?',
            icon: 'times-circle'
          }
        }
      } );
  
      var sub = modalRef.content.action.subscribe( (result: any) => { 
        sub.unsubscribe();
        this._provisionsService.newDestroy(provision._id.toString(), this.currentUser._id.toString()).subscribe( res => {
          console.log("openConfirmDestroyModal returns", result);
        });
      });
    }

}
