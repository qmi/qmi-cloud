import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit, OnDestroy {
  
  sections = ['provisions', 'scenarios',  'scenario-deploy-opts', 'users', 'notifications','sessions', 'api-keys', 'vm-types', 'webhooks'];
  tab : string =  'Provisions';
  private sub: any;

  constructor( private route: ActivatedRoute, private router: Router ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.tab = params['tab'] || 'provisions'; // (+) converts string 'id' to a number
    });
  }

  tabSelect($event, tab) {
    this.router.navigate(['/admin', tab]);
    /*$event.preventDefault();
    $event.stopPropagation();
    this.tab = tab;*/
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
