import { Component, AfterViewInit, Input } from '@angular/core';
import { QlikService } from './services/qs.service';
import { AuthGuard } from './services/auth.guard';
import { Router } from '@angular/router';

@Component({
  selector: 'my-qsembed',
  template: `<div (click)="goToCost()" style="width:400px; height: 80px;display: {{hide==='no'? 'block' : 'none'}}; position: absolute; right: 0; top: -20px;">
        <qlik-embed
            id="qsobject-data"
            ui="object"
            app-id="477a7c47-3326-427b-9cef-43f5db3787a0"
            object-id="XNJZpw"
            ></qlik-embed>
        </div>`
})

export class QsEmbedComponent implements AfterViewInit {
    
    @Input() user;
    @Input() hide;
    @Input() provision;

    private _currentUser;

    constructor(private _qlikService: QlikService, private _auth: AuthGuard, private router: Router ) {
        this._auth.getUserInfo().subscribe( value => {
            this._currentUser = value;
        });  
    }

    ngAfterViewInit(): void {
        
        var trigram = null;
        if ( this.user && this.user.upn ) {
            trigram = this.user.upn.split("@")[0].toUpperCase();
        }
        const qsEmbed = document.getElementById("qsobject-data");
        this._qlikService.setCostData(qsEmbed, this._currentUser, trigram, this.provision);
    }

    goToCost() {
        this.router.navigate(['cost-analysis']);
    }
}
