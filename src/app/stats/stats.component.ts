import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsersService } from '../services/users.service';
import { AuthGuard } from '../services/auth.guard';

declare var qlikMashup: any;

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})

export class StatsComponent implements OnInit, OnDestroy {
  
  currentURL;
  currentUser;


  constructor(private _usersService: UsersService, private _auth: AuthGuard) { 
    this.currentURL = window.location.href;
    this._auth.getUserInfo().subscribe( value => {
        this.currentUser = value;
      });  
  }

  ngOnInit() {

    const config = {
      host: "gear-presales.eu.qlikcloud.com",
      secure: true,
      port: 443,
      prefix: "",
      webIntegrationId: 'n4kMLH62hvXXC84q2vdfW15WUvrUw-HU',
      appId: "d8e47b57-19fb-4443-a73c-dcb179424ba3"
    };

    qlikMashup.initMyQdt( config, this.currentURL ).then(function(res) {

      
      var app = res.app;
      var qcsLoginUser = res.user;

      var patchData = {"qcsUserId": qcsLoginUser.id, "qcsUserSubject": qcsLoginUser.subject};
      this._usersService.updateUser(this.currentUser._id, patchData).subscribe(function(res){
        console.log("Done update user", res);
      });

      qlikMashup.myQdtViz(app, 'currentselections', 'CurrentSelections', 40);
      qlikMashup.myQdtViz(app, 'qdt1', 'VeMvQY', 300, 'table');
      qlikMashup.myQdtViz(app, 'qdt1a' ,'7fe2ada5-b798-4b2f-b381-ac0f12a3d30d', 300, 'piechart');
      qlikMashup.myQdtViz(app, 'qdt3', 'f15c052a-4e7d-4b6a-80ef-84254e634807', 300, 'linechart');
      qlikMashup.myQdtViz(app, 'qdt4', '2943f835-3eca-4c6c-a482-c109931f3627', 300, 'barchart');

      qlikMashup.myQdtViz(app, 'filterstatus', '84dd2221-edd5-4512-ba5c-f9f9110cf828', 40);
      
      qlikMashup.myQdtViz(app, 'totalprovkpi','990125da-bed1-4ffa-8ca8-b5e95c9a6c11', 100);
      qlikMashup.myQdtViz(app, 'costhismonth','ee3c5e79-e944-4789-bec2-6238b4f697c1', 100);
      qlikMashup.myQdtViz(app, 'statusprov','894d9009-9fcd-49db-b4a9-5bcdbeb8cd7e', 150);
      qlikMashup.myQdtViz(app, 'scenariosbreakdown','b26d29e6-7480-472d-adb7-f297751b2445', 300);
    
    }.bind(this));





  }

  ngOnDestroy() {
  }
    
}
