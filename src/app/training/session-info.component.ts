import { Component, OnInit, OnDestroy } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject, Subscription } from 'rxjs';
import { TrainingService } from '../services/training.service';
import { UsersService } from '../services/users.service';
import { AuthGuard } from '../services/auth.guard';

@Component({
  selector: 'qmi-info-session',
  templateUrl: './session-info.component.html',
  styleUrls: ['./session-info.component.scss']
})
export class SessionInfoModalComponent implements OnInit, OnDestroy {
  
  session;
  action: Subject<any> = new Subject();

  students;
 
  constructor( public modalRef: MDBModalRef, private _trainingService : TrainingService, private _usersService: UsersService, private _auth: AuthGuard) {
  }

  ngOnInit() {
    
    this._trainingService.getStudents(this.session._id).subscribe(result=> {
      this.students = result.results;
    });
    
  }

  ngOnDestroy() { 
    
  }

}