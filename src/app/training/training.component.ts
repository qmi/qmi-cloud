import { Component, OnInit } from '@angular/core';
import { TrainingService } from '../services/training.service';
import { AuthGuard } from '../services/auth.guard';
import { SessionModalComponent } from './edit-session.component';
import { MDBModalService } from 'angular-bootstrap-md';
import { Subscription } from 'rxjs';
import { SessionInfoModalComponent } from './session-info.component';

@Component({
  selector: 'training-component',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss']
})
export class TrainingComponent implements OnInit {

  private _userId;
  private _currentUser;
  sessions;
  templates;
  constructor( private modalService: MDBModalService, private _trainingService : TrainingService, private _auth: AuthGuard ) {
    this._auth.getUserInfo().subscribe( value => {
      this._userId = value? value._id : null;
      this._currentUser = value? value : null;
    });
   }

  ngOnInit(): void {

    this._trainingService.getTemplates().subscribe(res=>{

      this.templates = res.results;
    })

    this._refresh();
    
  }

  private _refresh() {
    var sub = this._trainingService.getTrainingSessions(this._userId).subscribe(res=>{
      this.sessions = res.results;
      sub.unsubscribe();
    });
  }

  newSession(template, session) {
    var modalRef = this.modalService.show(SessionModalComponent, {
      class: 'modal-lg',
      containerClass: '',
      data: {
        template: template,
        session: session,
        user: this._currentUser
      }
    } );
    
    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
      this._refresh();
    });
  }

  infoSession(session) {
    var modalRef = this.modalService.show(SessionInfoModalComponent, {
      class: 'modal-md',
      containerClass: '',
      data: {
        session: session
      }
    } );
    
    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
    });
  }

}
