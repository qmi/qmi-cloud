import { Component, OnInit, OnDestroy } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject, Subscription } from 'rxjs';
import { TrainingService } from '../services/training.service';
import { UsersService } from '../services/users.service';
import { AuthGuard } from '../services/auth.guard';

@Component({
  selector: 'qmi-new-session',
  templateUrl: './edit-session.component.html',
  styleUrls: ['./edit-session.component.scss']
})
export class SessionModalComponent implements OnInit, OnDestroy {
  
  template;
  session;
  user;
  action: Subject<any> = new Subject();
  subscriptions;
  selectedDeployOpts;
  selectedAllowedUsers;
  selectedAllowedUsersObjects = [];
  allUsers;
  labels : String = "";
  support: String = "";
  users;
  currentUser;
  selectedUser;

  sendData : any = {};
  forms;
 
  constructor( public modalRef: MDBModalRef, private _trainingService : TrainingService, private _usersService: UsersService, private _auth: AuthGuard) {
    this._auth.getUserInfo().subscribe( value => {
      this.currentUser = value;
    });
  }

  ngOnInit() {
    

    this.forms = document.getElementsByClassName('needs-validation');
    if (this.session) {
      this.sendData = JSON.parse(JSON.stringify(this.session));
      this.sendData.template = this.template._id;
    } else {
      this.sendData = {
        template: this.template._id
      }
    }
    this._usersService.getUsers(true, {"active": true}).subscribe(res=> {
      this.users = res.results;
      if ( this.session && this.session.user ) {
        this.selectedUser = this.session.user._id;
      } else {
        this.selectedUser = this.user._id;
      }
    });
    
  }

  ngOnDestroy() { 
    
  }

  confirm() : void {

    if (!this.selectedUser ) {
      this.forms[0].classList.add('was-validated');
      return;
    }

    if (!this.sendData.description ) {
      this.forms[0].classList.add('was-validated');
      return;
    }

    
    if (this.template.qcs && !this.sendData.qcsTenantHost) {
      this.forms[0].classList.add('was-validated');
      return;
    }

    if (this.template.needQcsApiKey && (!this.sendData.qcsApiKey )) {
      this.forms[0].classList.add('was-validated');
      return;
    }

    if (this.template.cloudshare && (!this.sendData.cloudshareClass)) {
      this.forms[0].classList.add('was-validated');
      return;
    }
    
    this.sendData.user = this.selectedUser;
    
    if (this.session && this.session._id){
      this._trainingService.updateSession(this.user._id, this.session._id, this.sendData).subscribe(result=>{
        this.action.next("DONE update!!!");
        this.modalRef.hide();
      })
    } else {
      this._trainingService.addSession(this.user._id, this.sendData).subscribe(result=>{
        this.action.next("DONE new!!!");
        this.modalRef.hide();
      })
    }

    
        
  }

  delete() : void {    
    this._trainingService.deleteSession(this.user._id, this.session._id).subscribe(result=>{
      this.action.next("DONE delete!!!");
      this.modalRef.hide();
    })
  }

  checkOnchange($event, field) {
    this.sendData[field] = $event.checked;
  }

  checkOnchangeStatus($event) {
    this.sendData.status = $event.checked? 'active': 'inactive';
  }

}