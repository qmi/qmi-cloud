import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TrainingService } from '../services/training.service';

@Component({
  selector: 'sessionform-root',
  templateUrl: './sessionform.component.html',
  styleUrls: ['./sessionform.component.css']
})
export class SessionFormComponent implements AfterViewInit, OnInit {
  id: string;
  hostname = "";
  session;
  sessionName = "";
  selectedSession = "";
  qrCodeUrl = "";
  code = "";
  email;
  captcha;
  forms;
  error: boolean = false;
  success: boolean = false;
  wait: boolean = false;
  status = "active";
  invalidemail: boolean = false;
  invalidcaptcha: boolean = false;

  @ViewChild('myCanvas')
  private myCanvas: ElementRef = {} as ElementRef;


  constructor(private _trainingService : TrainingService, private route: ActivatedRoute) {

    this.hostname = window.location.href;  
    this.qrCodeUrl = `https://quickchart.io/qr?size=300&text=${this.hostname}`;
  }

  ngOnInit(): void {
    this.forms = document.getElementsByClassName('needs-validation');
    this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number

      
      this._trainingService.getTrainingSessionDetails(this.id).subscribe(res=>{
        this.session = res;
        this.status = this.session.status;
      });
      
    });
  }
  ngAfterViewInit(): void {
    //clear the contents of captcha div first
    var charsArray =
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$%^&*";
    var lengthOtp = 6;
    var captcha = [];
    for (var i = 0; i < lengthOtp; i++) {
      //below code will not allow Repetition of Characters
      var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
      if (captcha.indexOf(charsArray[index]) == -1)
        captcha.push(charsArray[index]);
      else i--;
    }
    
    //if (this.myCanvas){
      var ctx = this.myCanvas.nativeElement.getContext('2d');
      if (ctx) {
        ctx.font = "36px Georgia";
        ctx.strokeStyle = "#009845";
        ctx.strokeText(captcha.join(""), 0, 30);
      }
    //}
    //storing captcha so that can validate you can save it somewhere else according to your specific requirements
    this.code = captcha.join("");
    
  }


  submitForm() {
    
    this.invalidemail = false;
    this.invalidcaptcha = false;

    if (!this.email ) {
      this.forms[0].classList.add('was-validated');
      return;
    }

    if (this.session.studentEmailFilter) {
      let filter = this.session.studentEmailFilter.toLowerCase();
      let emailDomain = this.email.split("@")[1].toLowerCase();
      if ( !filter.includes(emailDomain) ) {
        this.email = null;
        this.forms[0].classList.add('was-validated');
        this.invalidemail = true;
        return;
      }
    }

    if (this.captcha !== this.code) {
      this.invalidcaptcha = true;
      this.captcha = null;
    }
    if (!this.captcha) {
      this.forms[0].classList.add('was-validated');
      return;
    }



    this.wait = true;
    this._trainingService.submit(this.session._id, {email: this.email}).subscribe(result=>{
      this.wait = false;
      this.success = true;
    },err => {
      console.log("Error caught at Subscriber ",err);
      this.wait = false;
      this.error = true;
    })
    
  }



}
