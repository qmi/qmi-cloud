import { Component, OnInit, ElementRef, Input, OnDestroy } from '@angular/core';
import QdtComponents from 'qdt-components';

const qConfig = {
    "config": {
        "host": "gear-presales.eu.qlikcloud.com",
        "secure": true,
        "port": 443,
        "prefix": "",
        "webIntegrationId": "n4kMLH62hvXXC84q2vdfW15WUvrUw-HU",
        "appId": "2d03e11f-f3d8-4ba7-a123-be5a282fb9f1"
    },
    "connections": { 
        "vizApi": true, 
        "engineApi": true 
    }
}

@Component({
  selector: 'qdt-component',
  templateUrl: './qdt-components.component.html',
  styleUrls: ['./qdt-components.component.scss']
})
export class QdtComponentComponent implements OnInit, OnDestroy {

	@Input() Component: string;
  @Input() props: object;

  static qdtComponent;
    
	constructor(private elementRef: ElementRef) { 
    if ( !QdtComponentComponent.qdtComponent ) {
      QdtComponentComponent.qdtComponent = new QdtComponents(qConfig.config, qConfig.connections);
    }
  }

  ngOnInit() {
    QdtComponentComponent.qdtComponent.render(this.Component, this.props, this.elementRef.nativeElement);
  }

  ngOnDestroy() {
    //QdtComponentComponent.qdtComponent.unmountQdtComponent(this.elementRef.nativeElement)
  }

}