import { Component, OnInit, OnDestroy } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject, Subscription } from 'rxjs';
import { ScenariosService } from '../services/scenarios.service';
import { SubscriptionsService } from '../services/deployopts.service';
import { UsersService } from '../services/users.service';
import { AuthGuard } from '../services/auth.guard';

@Component({
  selector: 'qmi-new-scenario',
  templateUrl: './edit-scenario.component.html',
  styleUrls: ['./edit-scenario.component.scss']
})
export class ScenarioModalComponent implements OnInit, OnDestroy {
  
  scenario;
  action: Subject<any> = new Subject();
  subscriptions;
  selectedDeployOpts;
  currentUser;
  selectedAllowedUsers;
  selectedAllowedUsersObjects = [];
  allUsers;
  labels : String = "";
  support: String = "";

  sendData : any = {
    availableProductVersions: [{
      product: 'String: <ie: Qlik Sense>',
      vmTypeDefault: 'String: <i.e: Standard_D2s_v3>',
      diskSizeGbDefault: 'Integer: <values: 128,250,500,750,1000>',
      index: 'vm1',
      values: []
    }]
  };
 
  constructor( public modalRef: MDBModalRef, private _usersService: UsersService, private _scenariosService: ScenariosService, private _subscriptionsService: SubscriptionsService, private _auth: AuthGuard ) {}

  ngOnInit() {

      this._auth.getUserInfo().subscribe( value => {
        this.currentUser = value;
      });

      this._usersService.getUsers(true).subscribe( users => {
        this.allUsers = users.results;
      });

      this._subscriptionsService.getSubscriptions().subscribe ( res => {
        this.subscriptions = res.results;
        if (this.scenario) {
          this.sendData = JSON.parse(JSON.stringify(this.scenario))
        }
        

        if ( this.scenario.deployOpts ) {
          this.selectedDeployOpts = this.scenario.deployOpts.map( item => item._id);
        }

        if ( this.scenario.allowedUsers ) {
          this.selectedAllowedUsers = this.scenario.allowedUsers.map( item => item._id);
          this.selectedAllowedUsersObjects = JSON.parse(JSON.stringify(this.scenario.allowedUsers));
        }

        if ( this.scenario.labels ) {
          this.labels = this.scenario.labels.join(",");
        }

        if ( this.scenario.support ) {
          this.support = this.scenario.support.join(",");
        }
      });
  }

  getFlagName(subs): string {
    if ( subs.location === "West Europe" ) {
      return "&#127470;&#127466; "+subs.description;
    } else if (subs.location === "Southeast Asia") {
      return "&#127480;&#127468; "+subs.description;
    } else {
      return "&#127482;&#127480; "+subs.description;
    }
  }

  ngOnDestroy() { 
    
  }

  confirm() : void {
      let postData = this.sendData;
      postData.deployOpts = this.selectedDeployOpts; 
      postData.allowedUsers = this.selectedAllowedUsers; 
      
      if ( this.labels.length ) {
        postData.labels = this.labels.split(",");
      } 
      if ( this.support.length ) {
        postData.support = this.support.split(",");
      }
      if (!postData.isExternal){
        delete postData['forceExternalAccess'];
      }
      if ( postData._id) {
        var id = postData._id.toString();
        postData._id = undefined;
        this._scenariosService.updateScenario(id, postData).subscribe( res=> {
          this.action.next("DONE!!!");
          this.modalRef.hide();
        });
      } else {
        this._scenariosService.addScenario(postData).subscribe( res=> {
          this.action.next("DONE!!!");
          this.modalRef.hide();
        });
      }
      
      
  }

  delete() : void {    
    this._scenariosService.deleteScenario(this.sendData._id).subscribe( res=> {
      this.action.next("DONE!!!");
      this.modalRef.hide();
    });   
  }

  checkOnchange($event, field) {
    this.sendData[field] = $event.checked;
  }

  updateJson(event: any, property: string) {
    var editField = event.target.textContent.trim();
    try {
        var value = JSON.parse(editField);
        this.sendData[property] = value;

    } catch (e) {
        console.log("error json", e);
    }
  }
}