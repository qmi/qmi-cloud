import { Component, OnInit, OnDestroy } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject } from 'rxjs';
import { UsersService } from '../services/users.service';
import { ProvisionsService } from '../services/provisions.service';
import { AuthGuard } from '../services/auth.guard';

@Component({
  selector: 'qmi-share-provision',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.scss']
})
export class ShareModalComponent implements OnInit, OnDestroy {
  
  private _userId;
  provision;
  action: Subject<any> = new Subject();
  users;
  sharedUsers;
  selectedUser;
  acl;
 
  constructor( private _auth: AuthGuard, public modalRef: MDBModalRef, private _usersService: UsersService, private _provisionsService: ProvisionsService ) {
    this.acl = { "type": "view"};
    this._auth.getUserInfo().subscribe( value => {
      this._userId = value? value._id : null;
    });
  }

  refresh() : void {
    this._provisionsService.getSharesForProvision(this._userId, this.provision._id).subscribe(res=> {
      this.sharedUsers = res;   
    });
  }

  ngOnInit() {    
      this._usersService.getUsers(true, {"active": true}).subscribe(res=> {
        this.users = res.results;     
      }); 

      this.refresh();
  }

  ngOnDestroy() { 
    
  }

  share() : void {  
    
      //this.modalRef.hide();
      this._provisionsService.shareProvision(this._userId, this.provision._id, this.selectedUser, this.acl ).subscribe( res=>{
        this.refresh();
        this.acl = { "type": "view"};
        //this.modalRef.hide();
      });
  }

  stopShare(withUserId) : void {  

      
    //this.modalRef.hide();

    this._provisionsService.stopShareProvision(this._userId, this.provision._id, withUserId ).subscribe( res=>{
      this.refresh();
      //this.modalRef.hide();
    });
}

}