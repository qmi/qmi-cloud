import { Component, OnInit, OnDestroy } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject, Subscription } from 'rxjs';
import { ScenariosService } from '../services/scenarios.service';
import { SubscriptionsService } from '../services/deployopts.service';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'qmi-new-apikey',
  templateUrl: './edit-apikey.component.html',
  styleUrls: ['./edit-apikey.component.scss']
})
export class ApikeyModalComponent implements OnInit, OnDestroy {
  
  apiKey;
  action: Subject<any> = new Subject();
  users;
  selectedUser;

  sendData : any = {};
 
  constructor( public modalRef: MDBModalRef, private _usersService: UsersService ) {}

  ngOnInit() {
    this._usersService.getUsers(true, {"active": true}).subscribe(res=> {
      this.users = res.results;
      if (this.apiKey) {
        this.sendData = JSON.parse(JSON.stringify(this.apiKey))
      }
      if (this.apiKey.user ) {
        this.selectedUser = this.apiKey.user._id;
      }
    })
      
  }

  ngOnDestroy() { 
    
  }

  confirm() : void {
      
      this.sendData.user = this.selectedUser;

      this._usersService.addApikey(this.selectedUser, this.sendData).subscribe( res=> {
        this.action.next("DONE!!!");
        this.modalRef.hide();
      });
      
  }

}