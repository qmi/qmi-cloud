import { Component, OnInit, OnDestroy } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject } from 'rxjs';
import { UsersService } from '../services/users.service';
import { ProvisionsService } from '../services/provisions.service';
import * as moment from 'moment-timezone';
import { AuthGuard } from '../services/auth.guard';

@Component({
  selector: 'qmi-edit-provision',
  templateUrl: './edit-provision.component.html',
  styleUrls: ['./edit-provision.component.scss']
})
export class ProvisionModalComponent implements OnInit, OnDestroy {
  
  provision;
  action: Subject<any> = new Subject();
  users;
  currentUser;
  selectedUser;
  scheduleData: any = {
    is24x7: false,
    isStartupTimeEnable: false,
    startupTime: {
      "hour": 7,
      "minute": 0
    },
    shutdownTime: {
      "hour": 20,
      "minute": 0
    }
  };

  sendData : any = {};
 
  constructor( private _auth: AuthGuard, public modalRef: MDBModalRef, private _usersService: UsersService, private _provisionsService: ProvisionsService ) {
    
  }

  ngOnInit() {

    this.sendData.description = this.provision.description;

    this._auth.getUserInfo().subscribe( value => {
      
      this.currentUser = value;
      
      if ( this.currentUser.role === 'admin' || this.currentUser.role === 'superadmin' ) {
        
        this._usersService.getUsers(true).subscribe(res=> {
            this.users = res.results;
            if ( this.provision.user ) {
              this.selectedUser = this.provision.user._id;
            }     
        }); 
      }
      if ( this.provision.schedule ) {
        if ( this.provision.schedule.localeShutdownTime ) {
          this.scheduleData.shutdownTime = {
            "hour": parseInt(this.provision.schedule.localeShutdownTime.split(":")[0]),
            "minute": parseInt(this.provision.schedule.localeShutdownTime.split(":")[1])
          }
        }
        if ( this.provision.schedule.localeStartupTime ) {
          this.scheduleData.startupTime = {
            "hour": parseInt(this.provision.schedule.localeStartupTime.split(":")[0]),
            "minute": parseInt(this.provision.schedule.localeStartupTime.split(":")[1])
          }
        }
        
        this.scheduleData.is24x7 = this.provision.schedule.is24x7;
        this.scheduleData.isStartupTimeEnable = this.provision.schedule.isStartupTimeEnable;
      } else {
        this.scheduleData.is24x7 = true;
      }  
    
    });
  }

  ngOnDestroy() { 
    
  }

  confirm() : void {  

      if (!this.sendData.description || this.sendData.description === "") {
        return;
      }

      if ( this.currentUser.role === 'admin' || this.currentUser.role === 'superadmin' ) {
        this.sendData.user = this.selectedUser;
      }

      if ( (this.provision.vmImage && this.provision.vmImage.vm1 || this.provision.options && this.provision.options.vm1) && this.provision._scenarioDoc && this.provision._scenarioDoc.isDivvyEnabled ) {
        let scheduleUTC = this.getUTCTimes();
        this.sendData.scheduleData = this.provision.schedule || {};
        this.sendData.scheduleData.is24x7 = this.scheduleData.is24x7;
        this.sendData.scheduleData.isStartupTimeEnable = this.scheduleData.isStartupTimeEnable;
        this.sendData.scheduleData.utcTagStartupTime = scheduleUTC.tagStartup;
        this.sendData.scheduleData.utcTagShutdownTime = scheduleUTC.tagShutdown;
        this.sendData.scheduleData.localeStartupTime = this.scheduleData.startupTime.hour+":"+this._provisionsService.addZero(this.scheduleData.startupTime.minute);
        this.sendData.scheduleData.localeShutdownTime = this.scheduleData.shutdownTime.hour+":"+this._provisionsService.addZero(this.scheduleData.shutdownTime.minute);
        this.sendData.scheduleData.localTimezone = scheduleUTC.timezone;
        this.sendData.scheduleData.timezoneOffset = scheduleUTC.timezoneOffset;
      }


      this._provisionsService.updateProvisionUser(this.provision._id, this.provision.user._id, this.sendData ).subscribe( res=>{
        let user = this.users.filter( u => u._id === this.selectedUser);
        this.action.next({"user": user[0]});
        this.modalRef.hide();
      });
  }

  checkOnchangeSchedule($event) {
    this.scheduleData.is24x7 = $event.checked;
  }

  checkOnchangeStartupTime($event) {
    this.scheduleData.isStartupTimeEnable = $event.checked;
  }

  getUTCTimes() {
    const zone = this.provision.schedule && this.provision.schedule.localTimezone? this.provision.schedule.localTimezone : moment.tz.guess(true);
    return this._provisionsService.getUTCTimes(this.scheduleData, zone);
  }

}