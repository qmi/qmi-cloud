import { Component, OnInit, OnDestroy } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject, Subscription } from 'rxjs';
import { ScenariosService } from '../services/scenarios.service';
import { SubscriptionsService } from '../services/deployopts.service';
import { UsersService } from '../services/users.service';
import { AuthGuard } from '../services/auth.guard';

@Component({
  selector: 'qmi-new-webhook',
  templateUrl: './edit-webhook.component.html',
  styleUrls: ['./edit-scenario.component.scss']
})
export class WebhookModalComponent implements OnInit, OnDestroy {
  
  action: Subject<any> = new Subject();
  webhook;
  scenarios;
  eventTypes = ["provision.init", "provision.finished", "provision.error", "provision.destroy-init", "provision.destroy-finished", "provision.destroy-error"];
  currentUser;
  selectedAllowedUsers;
  selectedAllowedUsersObjects = [];
  allUsers;
  labels : String = "";
  support: String = "";

  sendData : any = {
    isEnabled : true,
    headers: {},
    eventType: "provision.finished"
  };
 
  constructor( public modalRef: MDBModalRef, private _usersService: UsersService, private _scenariosService: ScenariosService, private _subscriptionsService: SubscriptionsService, private _auth: AuthGuard ) {}

  ngOnInit() {

      this._auth.getUserInfo().subscribe( value => {
        this.currentUser = value;
      });

      this._scenariosService.getScenarios().subscribe( scenarios => {
        this.scenarios = scenarios.results;
      });

      if ( this.webhook ) {
        this.sendData = Object.assign({}, this.webhook); 
      } else {
        this.updateJson(null, "headers");
      }

     
      

  }


  ngOnDestroy() { 
    
  }

  confirm() : void {

    if (this.sendData.scenario === "") {
      this.sendData.scenario = undefined;
    }

    
    this._usersService.addWebhook(this.sendData).subscribe( res=> {
      this.action.next("DONE!!!");
      this.modalRef.hide();
    });
      
  }

  delete() : void {    
    this._usersService.delWebhook(this.sendData._id).subscribe( res=> {
      this.action.next("DONE!!!");
      this.modalRef.hide();
    });   
  }

  checkOnchange($event, field) {
    this.sendData[field] = $event.checked;
  }

  updateJson(event: any, property: string) {
    var editField = event? event.target.textContent.trim() : "{}" ;
    try {
        var value = JSON.parse(editField);
        this.sendData[property] = value;

    } catch (e) {
        console.log("error json", e);
    }
  }
}