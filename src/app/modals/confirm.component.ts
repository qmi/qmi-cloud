import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject } from 'rxjs';

@Component({
  selector: 'qmi-modalconfirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ModalConfirmComponent implements OnInit, OnDestroy {
  
  info;
  provision;
  action: Subject<any> = new Subject();
  isStartupTimeEnable = false;

  constructor( public modalRef: MDBModalRef ) {}

  ngOnInit() {
    if (!this.info.buttonColor) {
      this.info.buttonColor = "danger";
    }
    if ( this.provision && this.provision.schedule ) {
      this.isStartupTimeEnable = this.provision.schedule.isStartupTimeEnable;
    }
  }

  checkOnchange($event) {
    this.isStartupTimeEnable = $event.checked;
  }

  ngOnDestroy() { 
    
  }

  confirm() : void {
      this.action.next({"isStartupTimeEnable": this.isStartupTimeEnable});
      this.modalRef.hide();
  }

}
