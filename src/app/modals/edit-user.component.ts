import { Component, OnInit, OnDestroy } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject, Subscription } from 'rxjs';
import { TrainingService } from '../services/training.service';
import { UsersService } from '../services/users.service';
import { AuthGuard } from '../services/auth.guard';

@Component({
  selector: 'qmi-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class UserModalComponent implements OnInit, OnDestroy {
  
  userId;
  user;
  currentUser;
  roles = ['user', 'user-cost', 'admin'];
  selectedRole;
  action: Subject<any> = new Subject();
  sendData : any = {};
  groups = "-";
  forms;
 
  constructor( public modalRef: MDBModalRef, private _usersService: UsersService, private _auth: AuthGuard) {
    
  }

  ngOnInit() {
    
    if ( this.currentUser.role === 'superadmin' ) {
      this.roles.push('superadmin');
    }


    this.forms = document.getElementsByClassName('needs-validation');

    this._usersService.getUserById(this.userId).subscribe( user=> {
      this.user = user;
      this.groups = this.user.msGroups? this.user.msGroups.join(', ') : "-";
      this.selectedRole = this.user.role;
      this.sendData.jobTitle = this.user.jobTitle;
      this.sendData.flags = this.user.featureFlags? this.user.featureFlags.join(',') : "";
      this.sendData.active = this.user.active;
      this.sendData.displayName = this.user.displayName;
    });

  }

  ngOnDestroy() { 
    
  }

  confirm() : void {

    if (!this.selectedRole ) {
      this.forms[0].classList.add('was-validated');
      return;
    }

    this.sendData.role = this.selectedRole;
    this.sendData.featureFlags = this.sendData.flags.split(",");
    
    this._usersService.updateUser(this.user._id, this.sendData).subscribe(result=>{
      this.action.next(result);
      this.modalRef.hide();
    });   
        
  }

  onImgError(event, user){
    event.target.src = 'https://ui-avatars.com/api/?name='+user.displayName+'&size=40&background=00807b&color=fff'
  }


  refreshPicture():void {
    this._usersService.updateTitlePicture(this.user._id).subscribe(response=>{
      console.log("Picture Updated");
    })
  }
}