import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MDBModalRef, MDBModalService } from 'angular-bootstrap-md';
import { AlertService } from '../services/alert.service';
import { AuthGuard } from '../services/auth.guard';
import { ProvisionsService } from '../services/provisions.service';
import { ModalConfirmComponent } from './confirm.component';
import { QlikService } from '../services/qs.service';

@Component({
  selector: 'qmi-modalinfo',
  templateUrl: './modalinfo.component.html',
  styleUrls: ['./modalinfo.component.scss']
})
export class ModalInfoComponent implements OnInit, OnDestroy {
  
  provisionId;
  info;
  events;
  awsKey;
  currentUser;
  private _userId;
  sharedUsers;
  costData;

  constructor(  private modalService: MDBModalService, private _alertService: AlertService, private router: Router, public modalRef: MDBModalRef, private _provisionsService: ProvisionsService, private _auth: AuthGuard, private _qlikService: QlikService ) {
    this._auth.getUserInfo().subscribe( value => {
      this.currentUser = value;
      this._userId = value? value._id : null;
    });
  }

  private _refreshEvents() {
    var es = this._provisionsService.getEvents(this.info.user._id, this.provisionId).subscribe(res=> {
        this.events = res.results;
        es.unsubscribe();
    });
  }

  private _refreshShares() {
    var ss = this._provisionsService.getSharesForProvision(this.info.user._id, this.provisionId).subscribe(res=> {
        this.sharedUsers = res;  
        ss.unsubscribe(); 
    });
  }

  private _refreshAws() {
    var es2 = this._provisionsService.getAWSKey(this.info.user._id, this.provisionId).subscribe(res=> {
      console.log("awsKey", res);
      this.awsKey = (res && res.length > 0)? res[0] : null;
      if (this.awsKey && this.awsKey.ageInDays) {
        this.awsKey.ageInDays = Math.round(this.awsKey.ageInDays);
      }
      es2.unsubscribe();
    });
  }

  ngOnInit() {

    var mains = this._provisionsService.getProvisionUserById( this._userId, this.provisionId).subscribe( provision => {
      this.info = provision;
      this._refreshEvents();
      this._refreshShares();
      this._refreshAws();
      mains.unsubscribe();
    });

    this._qlikService.costSubject.subscribe(function(value){
      this.costData = value;
    }.bind(this));
    
  }

  getCost(id){
    return this.costData && this.costData[id]? this.costData[id].dollars : "n/a";
  }

  getFlag(provision) : string {
    var flag = "us";
    if (provision.deployOpts.location === "West Europe") {
      flag = "ie";
    } else if (provision.deployOpts.location === "Southeast Asia") {
      flag = "sg";
    }
    return flag;
  }

  ngOnDestroy() { 
    
  }

  getOptionName(key){
    if ( this.info && this.info._scenarioDoc && this.info._scenarioDoc.availableProductVersions){
      var found = this.info._scenarioDoc.availableProductVersions.find(function(opt) {
        return opt.index === key
      });
      if (found){
        return found.product? found.product : found.index;
      } else {
        return key;
      }
    } else {
      return key;
    }
  }

  navigateTo($event) {
    this.router.navigate(['/provision', this.provisionId]);
    this.modalRef.hide()
  }

  openRemoteAccess(item) {
    window.open(item.value, item.key);
  }

  rotateAWSKey(item) {
    var modalRef = this.modalService.show(ModalConfirmComponent, {
      class: 'modal-sm modal-notify modal-danger',
      containerClass: '',
      data: {
        info: {
          title: 'Confirm rotate this access key?',
          icon: 'tick'
        }
      }
    } );

    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
      this._provisionsService.rotateAWSKey(this._userId, item._id.toString()).subscribe( res => {
        console.log("res", res);
        item.outputs = res.outputs;
        this._refreshAws();
      });
    });

  }
  
  rotateSAKey(item) {

    var modalRef = this.modalService.show(ModalConfirmComponent, {
      class: 'modal-sm modal-notify modal-danger',
      containerClass: '',
      data: {
        info: {
          title: 'Confirm rotate this access key?',
          icon: 'tick'
        }
      }
    } );

    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
      this._provisionsService.rotateSAKey(this._userId, item._id.toString()).subscribe( res => {
        console.log("res", res);
        item.outputs = res.outputs;
      });
    });
  }

  onImgError(event, user){
    event.target.src = 'https://ui-avatars.com/api/?name='+user.displayName+'&size=40&background=00807b&color=fff'
  }

  openModalChangeSuccess(provision) {
    var modalRef = this.modalService.show(ModalConfirmComponent, {
      class: 'modal-sm modal-notify modal-danger',
      containerClass: '',
      data: {
        info: {
          title: 'Confirm change to a successful provision?',
          icon: 'tick'
        }
      }
    } );

    var sub = modalRef.content.action.subscribe( (result: any) => { 
      sub.unsubscribe();
      this._provisionsService.updateProvisionUser(provision._id.toString(), this._userId, {"status": "provisioned"}).subscribe( res => {
        console.log("res", res);
        provision.status = res.provision.status;
        this._alertService.showAlert({
          type: 'alert-dark', 
          text: `Provision status changed to  <b>'provisioned'</b>.`
        });
      });
    });
  }

}
