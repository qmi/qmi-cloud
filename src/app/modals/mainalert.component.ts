import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'qmi-mainalert',
    template: `<div><div *ngIf="alert === 'true'" class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close" (click)="closeAlert()"><span aria-hidden="true">&times;</span></button>
            Important: <strong>Customer Data</strong> is not allowed on any QMI Cloud resource.
        </div></div>`
})
export class MainAlertComponent implements OnInit {


    alert: string;


    constructor() {}

    ngOnInit(): void {
        this.alert = sessionStorage.getItem('cdisclaimer') || "true";
    }

    closeAlert() {
        sessionStorage.setItem('cdisclaimer', "false");
        this.alert = "false";
    }

}
