import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { HomeComponent } from './home/home.component';
import { ProvisionsComponent } from './provisions/provisions.component';
import { ProvisionsSharedComponent } from './provisions/provisions-shared.component';
import { AuthGuard } from './services/auth.guard';
import { ProvisionsService } from './services/provisions.service';
import { ScenariosService } from './services/scenarios.service';
import { UsersService } from './services/users.service';
import { QlikService } from './services/qs.service';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MarkdownModule, MarkedOptions, MarkedRenderer } from 'ngx-markdown';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LogsComponent } from './logs/logs.component';
import { ScenariosComponent } from './scenarios/scenarios.component';
import { ScenariosSectionComponent } from './scenarios/scenarios-section.component';
import { AdminComponent } from './admin/admin.component';
import { PopoverconfirmComponent } from './popoverconfirm/popoverconfirm.component';
import { FormsModule } from '@angular/forms';
import { MyHttpInterceptor } from './interceptors/http.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TableProvisionsAdminComponent } from './tables/table-provisions.component';
import { TableScenariosComponent } from './tables/table-scenarios.component';
import { TableUsersComponent } from './tables/table-users.component';
import { TableWebhooksComponent } from './tables/table-webhooks.component';
import { TableNotificationsComponent } from './tables/table-notifications.component';
import { AlertComponent } from './modals/alert.component';
import { AlertService } from './services/alert.service';
import { ModalInfoComponent } from './modals/modalinfo.component';
import { ModalConfirmComponent } from './modals/confirm.component';
import { FilterPipe } from './filter.pipe';
import { FaqComponent } from './faq/faq.component';
import { NewProvisionConfirmComponent } from './modals/new-provision.component';
import { ScenarioModalComponent } from './modals/edit-scenario.component';
import { WebhookModalComponent } from './modals/edit-webhook.component';
import { ShareModalComponent } from './modals/share.component';
import { SubscriptionModalComponent } from './modals/edit-subscription.component';
import { TableSubsComponent } from './tables/table-subscriptions.component';
import { TableVmTypesComponent } from './tables/table-vmtypes.component';
import { CostComponent } from './cost/cost.component';
import { QsEmbedComponent } from './qsembed.component';

import { EnvServiceProvider } from './env.service.provider';
import { SubscriptionsService } from './services/deployopts.service';
import { StatsService } from './services/stats.service';
import { TableApiKeysComponent } from './tables/table-apikeys.component';
import { ApikeyModalComponent } from './modals/edit-apikey.component';
import { VmTypeModalComponent } from './modals/edit-vmtype.component';
import { StatsComponent } from './stats/stats.component';
import { TrainingComponent } from './training/training.component';
//import { QdtComponentComponent } from './qdt-components/qdt-components.component';
import { ProvisionModalComponent } from './modals/edit-provision.component';
import { SessionModalComponent } from './training/edit-session.component';
import { UserDashboardComponent } from './user/user-dashboard.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProvComponent } from './provisions/prov.component';
import { TrainingService } from './services/training.service';
import { SessionFormComponent } from './sessionform/sessionform.component';
import { FeatureGuard } from './services/feature.guard';
import { TableSessionsComponent } from './tables/table-sessions.component';
import { UserModalComponent } from './modals/edit-user.component';
import { SessionInfoModalComponent } from './training/session-info.component';
import { ErrorComponent } from './home/error.component';
import { BrowserService } from './services/browser.service';
import { SnapshotsService } from './services/snapshots.service';
import { MainAlertComponent } from './modals/mainalert.component';



export function markedOptions(): MarkedOptions {
  const renderer = new MarkedRenderer();

  renderer.blockquote = (text: string) => {
    return '<blockquote class="blockquote"><p>' + text + '</p></blockquote>';
  };

  return { renderer };
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorComponent,
    ProvisionsComponent,
    ProvisionsSharedComponent,
    LogsComponent,
    ScenariosComponent,
    ScenariosSectionComponent,
    AdminComponent,
    PopoverconfirmComponent,
    TableProvisionsAdminComponent,
    TableUsersComponent,
    TableWebhooksComponent,
    AlertComponent,
    ModalInfoComponent,
    ModalConfirmComponent,
    FilterPipe,
    FaqComponent,
    NewProvisionConfirmComponent,
    TableScenariosComponent,
    TableNotificationsComponent,
    ScenarioModalComponent,
    WebhookModalComponent,
    SubscriptionModalComponent,
    ShareModalComponent,
    TableSubsComponent,
    TableApiKeysComponent,
    ApikeyModalComponent,
    TableVmTypesComponent,
    VmTypeModalComponent,
    StatsComponent,
    //QdtComponentComponent,
    ProvisionModalComponent,
    UserDashboardComponent,
    CostComponent,
    ProvComponent,
    TrainingComponent,
    SessionFormComponent,
    SessionModalComponent,
    SessionInfoModalComponent,
    UserModalComponent,
    TableSessionsComponent,
    QsEmbedComponent,
    MainAlertComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UiModule,
    HttpClientModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),
    MarkdownModule.forRoot({
      loader: HttpClient
    }),
    NgbModule,
  ],
  providers: [
    EnvServiceProvider,
    { provide: HTTP_INTERCEPTORS, useClass: MyHttpInterceptor, multi: true },
    ProvisionsService, 
    ScenariosService, 
    SubscriptionsService,
    UsersService, 
    AlertService,
    AuthGuard,
    FeatureGuard,
    StatsService,
    TrainingService,
    QlikService,
    BrowserService,
    SnapshotsService
  ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
