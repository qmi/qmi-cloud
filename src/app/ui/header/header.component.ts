import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthGuard } from '../../services/auth.guard';
import { Subscription } from 'rxjs';

const _adminRoles = ['superadmin', 'admin'];

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  user;
  subs: Subscription;

  constructor( private _auth: AuthGuard){
    this.subs = this._auth.getUserInfo().subscribe( value => {
      this.user = value;
    });
  }

  ngOnInit() {
  
  }

  logout($event): void {
    $event.preventDefault();
    $event.stopPropagation();
    window.location.href = "/logout";
    this._auth.clearUser();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onImgError(event, user){
    event.target.src = 'https://ui-avatars.com/api/?name='+user.displayName+'&size=40&background=00807b&color=fff'
  }

  private _isAdmin(user) : boolean {
    return user && _adminRoles.includes(user.role);
  }

  checkFeature(user, feature) : boolean {
    if ( this._isAdmin(user) ) {
      return true;
    } else if ( user && user.featureFlags ) {
      return user.featureFlags.includes(feature);
    } else {
      return false;
    }
  }

}
