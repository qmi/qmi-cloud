import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthGuard } from 'src/app/services/auth.guard';


const _adminRoles = ['superadmin', 'admin'];

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit,OnDestroy {

  @Input() header;
  @Input() revision;

  user;
  subs: Subscription;

  constructor( private _auth: AuthGuard){
    this.subs = this._auth.getUserInfo().subscribe( value => {
      this.user = value;
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  private _isAdmin(user) : boolean {
    return user && _adminRoles.includes(user.role);
  }

  checkFeature(user, feature) : boolean {
    if ( this._isAdmin(user) ) {
      return true;
    } else if ( user && user.featureFlags ) {
      return user.featureFlags.includes(feature);
    } else {
      return false;
    }
  }

}
