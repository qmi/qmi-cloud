const mongoose = require('mongoose');
const { scenario } = require('../mongo');

const schema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    name: {
        type: String
    },
    owner: {
        type: mongoose.Types.ObjectId, 
        ref: 'User'
    },
    eventType: {
        type: String // provision.finished, provision.error, provision.destroyed-finished, provision.destroy-error
    },
    scenario: {
        type: String // scenario name
    },
    url: {
        type: String
    },
    headers: {
        type: Object,
        default: {}
    },
    queryparams: {
        type: Object
    },
    isEnabled: {
        type: Boolean,
        default: true
    },
    lastExecTs: {
        type: Date,
        default: Date.now
    },
    lastExecResult: {
        type: String
    },
    lastExecPayload: {
        type: String
    }
});


module.exports = mongoose.model('Webhook', schema)