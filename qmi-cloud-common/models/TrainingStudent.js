const mongoose = require('mongoose');


const schema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now
    },
    email: {
        type: String
    },
    session: {
        type: mongoose.Types.ObjectId, ref: 'TrainingSession'
    },
    status: {
        type: String,
        default: 'pending'
    }
});


module.exports = mongoose.model('TrainingStudent', schema);