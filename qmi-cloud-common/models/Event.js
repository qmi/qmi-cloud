const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    user: {
        type: mongoose.Types.ObjectId, 
        ref: 'User'
    },
    provision: {
        type: mongoose.Types.ObjectId, 
        ref: 'Provision',
        index : true
    },
    type: {
        type: String
    },
    message: {
        type: String
    }
});


module.exports = mongoose.model('Event', schema)