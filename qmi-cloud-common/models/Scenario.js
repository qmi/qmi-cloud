const mongoose = require('mongoose')


const scenarioSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    updated: {
        type: Date,
        default: Date.now
    },
    version: {
        type: String,
        required: true
    },
    name: {
        type: String,
        unique : true,
        required: true,
        index: true
    },
    isAdminOnly: {
        type: Boolean,
        default: false
    },
    isWafPolicyAppGw: {
        type: Boolean,
        default: false
    },
    isExternal: {
        type: Boolean,
        default: false
    },
    forceExternalAccess: {
        type: Boolean,
        default: false
    },
    isDivvyEnabled: {
        type: Boolean,
        default: false
    },
    numSimultaneousProvisions: {
        type: Number
    },
    isDisabled: {
        type: Boolean,
        default: false
    },
    title: {
        type: String,
        required: true
    },
    gitBranch: {
        type: String
    },
    description: String,
    availableProductVersions: Array,
    productVersionDefault: String,
    newImageName: String, //For Gen scenarios
    subscription: {
        type: mongoose.Types.ObjectId, 
        ref: 'Subscription'
    },
    deployOpts: [{
        type: mongoose.Types.ObjectId, 
        ref: 'Subscription',
        required: true
    }],
    labels: [{
        type: String
    }],
    allowedUsers: [{
        type: mongoose.Types.ObjectId, 
        ref: 'User',
        required: true
    }],
    support: [{
        type: String
    }],
    barracudaTemplate: {
        type: String
    },
    allowedInnactiveDays: {
        type: Number,
        default: 20
    },
    allowed24x7RunningDays: {
        type: Number,
        default: 7
    },
    allowedOnScheduleRunningDays: {
        type: Number,
        default: 4
    },
    terraformImage: {
        type: String
    },
    permittedGroups: {
        type: Array
    }
});


module.exports = mongoose.model('Scenario', scenarioSchema)