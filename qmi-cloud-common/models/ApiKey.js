const mongoose = require('mongoose');
const crypto = require("crypto");


const schema = new mongoose.Schema({
    user: {
        type: mongoose.Types.ObjectId, ref: 'User'
    },
    description: {
        type: String
    },
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    updated: {
        type: Date,
        default: Date.now
    },
    isActive: {
        type: Boolean,
        default: true
    },
    apiKey: {
        type: String,
        default: function() {
            return crypto.randomBytes(64).toString('hex');
        },
        index: true
    }
});


module.exports = mongoose.model('ApiKey', schema)