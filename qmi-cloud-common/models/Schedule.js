const mongoose = require('mongoose');


const sc = new mongoose.Schema({
    is24x7: {
        type: Boolean,
        required: true,
        default: true
    },
    isStartupTimeEnable: {
        type: Boolean,
        default: true
    },
    localeShutdownTime: {
        type: String
    },
    localeStartupTime: {
        type: String
    },
    localTimezone: {
        type: String
    },
    utcTagShutdownTime: {
        type: String
    },
    utcTagStartupTime: {
        type: String
    },
    timezoneOffset: {
        type: Number
    },
    weekStartDay: {
        type: Number,
        default: 1
    }
});


module.exports = mongoose.model('Schedule', sc)