const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    index: {
        type: String
    },
    created: {
        type: Date,
        default: Date.now
    },
    title: {
        type: String
    },
    description: {
        type: String
    },
    publicDescription: {
        type: String
    },
    cloudshare: {
        type: Boolean
    },
    qcs: {
        type: Boolean
    },
    needQcsAutomation: {
        type: Array //['main']
    },
    needQcsApiKey: {
        type: Boolean
    }     
});


module.exports = mongoose.model('TrainingTemplate', schema);