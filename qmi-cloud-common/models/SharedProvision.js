const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    updated: {
        type: Date,
        default: Date.now
    },
    user: {
        type: mongoose.Types.ObjectId, 
        ref: 'User',
        index: true
    },
    provision: {
        type: mongoose.Types.ObjectId, 
        ref: 'Provision',
        index: true
    },
    sharedWithUser: {
        type: mongoose.Types.ObjectId, 
        ref: 'User',
        index: true
    },
    canManage: {
        type: Boolean,
        default: false
    }
});


module.exports = mongoose.model('sharedProvision', schema);