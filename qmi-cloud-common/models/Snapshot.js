const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    resourceID: {
        type: String
    },
    name: {
        type: String
    },
    targetRg: {
        type: String
    },
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    status: {
        type: String,
        default: "created" //copy.init //copy.finished
    },
    provision: {
        type: mongoose.Types.ObjectId, 
        ref: 'Provision',
        index : true
    },
    owner: {
        type: mongoose.Types.ObjectId, 
        ref: 'User',
        index : true
    }
});


module.exports = mongoose.model('Snaphot', schema)