const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    type: String,
    desc: String,
    costHour: Number,
    disabled: {
        type: Boolean,
        default: false
    }
});


module.exports = mongoose.model('VmType', userSchema);