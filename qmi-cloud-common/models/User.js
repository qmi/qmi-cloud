const mongoose = require('mongoose')


const userSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    updated: {
        type: Date,
        default: Date.now
    },
    displayName: String,
    upn: {
        type: String,
        index: true
    },
    sub: String,
    oid: {
        type: String,
        index: true
    },
    role: {
        type: String,
        default: "user"
    },
    lastLogin: {
        type: Date
    },
    qcsUserId: {
        type: String
    },
    qcsUserSubject: {
        type: String
    },
    active: {
        type: Boolean,
        default: true
    },
    jobTitle: {
        type: String
    },
    mail: {
        type: String
    },
    featureFlags: {
        type: Array
    },
    msGroups: {
        type: Array
    }
});


module.exports = mongoose.model('User', userSchema);