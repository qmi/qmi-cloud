const mongoose = require('mongoose');
const crypto = require("crypto");


const schema = new mongoose.Schema({
    description: {
        type: String
    },
    user: {
        type: mongoose.Types.ObjectId, 
        ref: 'User'
    },
    template: {
        type: mongoose.Types.ObjectId, 
        ref: 'TrainingTemplate'
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    },
    status: {
        type: String,
        default: "inactive" //active, inactive, terminated
    },
    qcsTenantHost: {
        type: String
    },
    qcsApiKey: {
        type: String
    },
    qaUrl: {
        type: String
    },
    qaToken: {
        type: String
    },
    cloudshareClass: {
        type: String
    },
    qcsSharedSpace: {
        type: String
    },
    qcsDataSpace: {
        type: String
    },
    passwd: {
        type: String,
        default: function() {
            return crypto.randomBytes(4).toString('hex');
        }
    },
    studentsCount: {
        type: Number,
        default: 0
    },
    studentEmailFilter: {
        type: String // qlik.com,talend.com,gmail.com 
    }  
    
});


module.exports = mongoose.model('TrainingSession', schema);