const mongoose = require('mongoose')

const subSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    updated: {
        type: Date,
        default: Date.now
    },
    subsId: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    vnetExists: {
        type: Boolean,
        required: true,
        default: false
    },
    subnetId: {
        type: String
    },
    appGwSubnetId: {
        type: String
    },
    location: {
        type: String
    },
    wafPolicyName: {
        type: String
    },
    wafPolicyRgName: {
        type: String
    }
});


module.exports = mongoose.model('Subscription', subSchema);