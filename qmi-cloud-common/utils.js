const db = require("./mongo");
const sendEmail = require("./send-email");

async function afterStopVms( provision, triggerUserId, auto = false, type = 'vms' ) {
    let timeRunning = db.utils.getNewTimeRunning(provision);
    const dateNow = new Date();
    let patch = {
        "statusVms": "Stopped", 
        "timeRunning": timeRunning, 
        "stoppedFrom": dateNow, 
        "pendingNextAction": null
    };    
    
    if ( auto && provision._scenarioDoc ) { //From CLI (auto stop)
        let msg = `[CLI] TotalTimeRunning: ${timeRunning} mins`;
        // Actual onschedule reset
        if ( provision.schedule && !provision.schedule.is24x7 ) {
            patch["startDateOnSchedule"] = dateNow;
            patch["endDateOnSchedule"] = dateNow;
            msg += " - (Schedule) accumlated running time has been reset.";
        } else {
            msg += " - (24x7) accumlated running time was reached.";
        }

        await db.provision.update(provision._id.toString(), patch);
        await sendEmail.sendVMsStopped(provision, provision._scenarioDoc);
        db.event.add({ provision: provision._id, type: 'vms.stop', message: msg });
    
    } else { //On Demand stop
        

        if ( provision.schedule && !provision.schedule.is24x7 ) {
            patch["endDateOnSchedule"] = dateNow;

            //This is temporary, only to make sure there is some initial value soon
            if ( !provision["startDateOnSchedule"] ) {
                patch["startDateOnSchedule"] = dateNow;
            }
            
        }
        await db.provision.update(provision._id.toString(), patch);
        db.event.add({ user: triggerUserId, provision: provision._id, type: `${type}.stop`, message: `[Manual] TotalTimeRunning: ${timeRunning} mins` });
    }
}

async function afterStartVms( provision, triggerUserId, type = 'vms' ) {
    const dateNow = new Date();
    let countExtend = db.utils.getNewCountExtend(provision);
    var patch = {
        "statusVms": "Running", 
        "runningFrom": dateNow, 
        "countExtend": countExtend, 
        "pendingNextAction": null
    };

    // Actual onschedule reset
    
    let msg = `[Manual] TotalTimeRunning: ${provision.timeRunning} mins`;
    if ( provision.schedule && !provision.schedule.is24x7 ) {
        msg += " - Schedule time has been reset.";
        patch["startDateOnSchedule"] = dateNow;
        patch["endDateOnSchedule"] = dateNow;
    } else {
        msg += ` - 24x7, New count extend: ${countExtend}`;
    } 

    await db.provision.update(provision._id.toString(), patch);

    db.event.add({ user: triggerUserId, provision: provision._id, type: `${type}.start`, message: msg });

}

module.exports.afterStopVms = afterStopVms;
module.exports.afterStartVms = afterStartVms;