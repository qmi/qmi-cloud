
const fs = require('fs');
var config = JSON.parse(fs.readFileSync("/app/config/qmi-config.json").toString());


const qmiConfig = function () {
    return config;
}

fs.watch("/app/config/qmi-config.json", function(eventType, filename){
  let newData = fs.readFileSync("/app/config/qmi-config.json");
  config = JSON.parse(newData.toString());
});

module.exports.qmiConfig = qmiConfig;
