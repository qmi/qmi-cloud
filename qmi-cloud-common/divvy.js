const db = require("./mongo");
const axios = require('axios');
const MYQUEUES = require('./queues');
const queues = MYQUEUES.queues;
const SYNAPSE_QUEUE = MYQUEUES.SYNAPSE_QUEUE;

const onEvent = async function (event, user) {

    const dateNow = new Date();
    const now = dateNow.toISOString();
    const logEvent = (event && event.logEvent)? event.logEvent : 'DivvyCloud';
    const cloudName = event.cloudName;
    const vmName = event.vmName || "Unknown";
    const instanceState = event.instanceState;

    try {

        if (event.cloudName.toLowerCase().includes('qmi')) {
            
            if (event.provID && event.provID !== 'None') {

                var provision = await db.provision.getById(event.provID);
                        
                if (provision) {

                    var type = "vms";
                    if (logEvent.indexOf("RDS") !== -1) {
                        type = "db";
                    }

                    var id = provision._id.toString();

                    if ( provision.isDestroyed || provision.isDeleted ) {
                        console.log(`${now}# ${cloudName}||${logEvent}||${vmName}||${instanceState}: provId: '${id}' - Is destroyed!! --> Do nothing.`);
                        return;
                    }

                    if (provision.status === 'provisioned' || provision.status === 'error') {

                        if (event.instanceState === 'Stopped') {

                            //Check active children provisions
                            var children = await db.provision.get({ "parent": provision._id, "isDestroyed": false, "isDeleted": false });
                            if (children.results.length > 0) {

                                children.results.forEach(function (child) {
                                    if (child.scenario === 'azqmi-synapse') {
                                        queues[SYNAPSE_QUEUE].add("synapse_job", {
                                            provId: child._id,
                                            user: user,
                                            tasktype: 'pause'
                                        });
                                    }
                                });
                            }

                            if (provision.statusVms === 'Stopped') {
                                console.log(`${now}# ${cloudName}||${logEvent}||${vmName}||${instanceState}: provId: '${id}' - Already Stopped!! --> Do nothing.`);
                            } else {

                                let timeRunning = db.utils.getNewTimeRunning(provision);
                                let patch = {
                                    "statusVms": "Stopped",
                                    "timeRunning": timeRunning,
                                    "stoppedFrom": dateNow,
                                    "pendingNextAction": null
                                };

                                if (provision.schedule && !provision.schedule.is24x7) {
                                    patch["endDateOnSchedule"] = dateNow;

                                    //This is temporary, only to make sure there is value
                                    if (!provision["startDateOnSchedule"]) {
                                        patch["startDateOnSchedule"] = dateNow;
                                    }
                                }

                                await db.provision.update(id, patch);
                                console.log(`${now}# ${cloudName}||${logEvent}||${vmName} to ${instanceState}: provId: '${id}' --> changed to Stopped!!`);
                                db.event.add({ provision: provision._id, type: `${type}.stop`, message: `[Divvy] TotalTimeRunning: ${timeRunning} mins` });
                            }

                        } else if (event.instanceState === 'Running') {

                            if (provision.statusVms === 'Running') {
                                console.log(`${now}# ${cloudName}||${logEvent}||${vmName}||${instanceState}: provId: '${id}' - Already Running!! --> Do nothing.`);
                            } else {
                                let patch = {
                                    "statusVms": "Running",
                                    "runningFrom": dateNow,
                                    "pendingNextAction": null
                                };

                                // This is temporary, only to make sure there are values soon
                                if (provision.schedule && !provision.schedule.is24x7) {
                                    if (!provision["startDateOnSchedule"]) {
                                        patch["startDateOnSchedule"] = dateNow;
                                        patch["endDateOnSchedule"] = dateNow;
                                    }
                                }

                                await db.provision.update(id, patch);
                                console.log(`${now}# ${cloudName}||${logEvent}||${vmName}||${instanceState}: provId '${id}' --> Changed to Running!`);

                                db.event.add({ provision: provision._id, type: `${type}.start`, message: `[Divvy] TotalTimeRunning: ${provision.timeRunning} mins` });
                            }
                        }

                    } else {
                        console.log(`${now}# ${cloudName}||${logEvent}||${vmName}||${instanceState}: provId '${event.provID}' - Not yet 'provisioned' --> Do nothing.`);
                    }

                } else {
                    console.log(`${now}# ${cloudName}||${logEvent}||${vmName}||${instanceState}: provId '${event.provID}' - Provision not found --> Sending to QMI-DEV`);

                    axios({
                        method: 'post',
                        url: 'https://qmicloud-dev.qliktech.com/api/v1/divvy/events',
                        headers: { 'QMI-ApiKey': "027a77d4235da3d4613f930e8f8a405e0941a37d82e88234a0717633c2ffe2470bba9880705206df02df6368defa0e73274d6aabfbe14eaee7a921854a4e83b5" },
                        data: event
                    }).catch(function (err2) {
                        console.log('Error# ', err2);
                    });

                }

            } else {
                console.log(`${now}# ${cloudName}||${logEvent}||${vmName}||${instanceState}: provID attribute is missing.`);
            }

        } else {
            console.log(`${now}# ${cloudName}||${logEvent}||${vmName}||${instanceState}: event not for QMI' --> Do nothing.`);
        }

    } catch (error) {
        console.log(`${now}# ${cloudName}||${logEvent}||${vmName}||${instanceState}: error!!!!`, error);
    }
}

async function isApiKeyAuthenticated(req, res, next) {
    let key = req.query.apiKey || req.get('QMI-ApiKey');
    if (key) {
        var result = await db.apiKey.getOne({ "apiKey": key });
        if (result) {
            req.user = result.user;
            return next();
        } else {
            res.status(401).send({ "error": "Unauthorized" });
        }
    } else {
        res.status(401).send({ "error": "Unauthorized" });
    }
}


module.exports.onEvent = onEvent;
module.exports.isApiKeyAuthenticated = isApiKeyAuthenticated;