
const Queue = require('bull');

const TF_APPLY_QUEUE = 'TF_APPLY_QUEUE';
const TF_DESTROY_QUEUE = 'TF_DESTROY_QUEUE';
const TF_APPLY_QSEOK_QUEUE = 'TF_APPLY_QSEOK_QUEUE';
const STOP_CONTAINER_QUEUE = 'STOP_CONTAINER_QUEUE';
const SYNAPSE_QUEUE = 'SYNAPSE_QUEUE';
const WEBHOOK_QUEUE = 'WEBHOOK_QUEUE';


var terraformApplyQueue = new Queue(TF_APPLY_QUEUE, process.env.REDIS_URL);
var terraformDestroyQueue = new Queue(TF_DESTROY_QUEUE, process.env.REDIS_URL);
var terraformApplyQseokQueue = new Queue(TF_APPLY_QSEOK_QUEUE, process.env.REDIS_URL);
var stopContainerQueue = new Queue(STOP_CONTAINER_QUEUE, process.env.REDIS_URL);
var synapseQueue = new Queue(SYNAPSE_QUEUE, process.env.REDIS_URL);
var webhookQueue = new Queue(WEBHOOK_QUEUE, process.env.REDIS_URL);


const queues = {
  [TF_APPLY_QUEUE]: terraformApplyQueue,
  [TF_DESTROY_QUEUE]: terraformDestroyQueue,
  [TF_APPLY_QSEOK_QUEUE]: terraformApplyQseokQueue,
  [STOP_CONTAINER_QUEUE]: stopContainerQueue,
  [SYNAPSE_QUEUE]: synapseQueue,
  [WEBHOOK_QUEUE]: webhookQueue
};


for (let key in queues) {
  queues[key].on('completed', function(job, result) {
    //console.log(`Queues# Job ${job.id} completed! Result`, result);
    console.log(`Queue ${key}# Job ${job.id} completed!`);
  });
  
  
  queues[key].on('error', function(error) {
    // An error occured.
  });
  
  queues[key].on('waiting', function(jobId){
    console.log(`Queue ${key}# Job ${jobId} is waiting...`);
    // A Job is waiting to be processed as soon as a worker is idling.
  });
  
  queues[key].on('active', function(job, jobPromise){
    // A job has started. You can use `jobPromise.cancel()`` to abort it.
    console.log(`Queue ${key}# Job ${job.id} is now active`);
  });
  
  queues[key].on('stalled', function(job){
    // A job has been marked as stalled. This is useful for debugging job
    // workers that crash or pause the event loop.
  });
  
  queues[key].on('progress', function(job, progress){
    // A job's progress was updated!
    console.log(`Queue ${key}# Job ${job.id} is ${progress * 100}% ready!`);
  });
  
  queues[key].on('failed', function(job, err){
    // A job failed with reason `err`!
    console.log(`Queue ${key}# Job ${job.id} has failed:`, err);
  });
}


module.exports.TF_APPLY_QUEUE = TF_APPLY_QUEUE;
module.exports.TF_DESTROY_QUEUE = TF_DESTROY_QUEUE;
module.exports.TF_APPLY_QSEOK_QUEUE = TF_APPLY_QSEOK_QUEUE;
module.exports.STOP_CONTAINER_QUEUE = STOP_CONTAINER_QUEUE;
module.exports.SYNAPSE_QUEUE = SYNAPSE_QUEUE;
module.exports.WEBHOOK_QUEUE= WEBHOOK_QUEUE;
module.exports.queues = queues;



