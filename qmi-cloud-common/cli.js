const awscli = require("./awscli");
const azurecli = require("./azurecli");
const db = require("./mongo");


async function deallocate(provId, userId, isSendEmailAfter ) {
    try {
        let provision = await db.provision.getById(provId);
        if ( !provision ) return;

        if (provision.scenario === 'azqmi-fort'){
            return awscli.deallocate(provision, userId, isSendEmailAfter);
        } else {
            return azurecli.deallocate(provision, userId, isSendEmailAfter);
        }
    } catch (err) {
        console.log("CLI# ERROR stopping VMs", err);
    }
}

async function start(provId, userId){
    
    try {
        let provision = await db.provision.getById(provId);

        if ( !provision ) return;

        if (provision.scenario === 'azqmi-fort'){
            return awscli.start(provision, userId);
        } else {
            return azurecli.start(provision, userId);
        }
    } catch (err) {
        console.log("CLI# ERROR starting VMs", err);
    }
}

async function updateVmsTags(provId, tagsEdit) {
    try {
        return azurecli.updateVmsTags(provId, tagsEdit);
    } catch (err) {
        console.log("CLI# ERROR updateVmsTags", err);
    }
}

async function stopDb(provId, userId, isSendEmailAfter) {
    try {
        let provision = await db.provision.getById(provId);
        if ( !provision ) return;

        return awscli.stopDbInstance(provision, userId, isSendEmailAfter);
    } catch (err) {
        console.log("CLI# ERROR stopping DB", err);
    }
}

async function startDb(provId, userId) {
    try {
        let provision = await db.provision.getById(provId);
        if ( !provision ) return;

        return awscli.startDbInstance(provision, userId);
    } catch (err) {
        console.log("CLI# ERROR stopping DB", err);
    }
}

async function createSnapshots(provId, targetRg, userId) {
    try {
        return await azurecli.createSnapshots(provId, targetRg, userId);
    } catch (err) {
        console.log("CLI# ERROR createSnapshots", err);
    }
}

async function rotateStorageAccountKey(provId, keyName) {
    try {
        let provision = await db.provision.getById(provId);
        if ( !provision ) return;

        return await azurecli.rotateStorageAccountKey(provision, keyName);
    } catch (err) {
        console.log("CLI# ERROR rotateStorageAccountKey", err);
    }
}

async function rotateIAMUserAccessKey(provision) {
    try {
        return await awscli.rotateAccessKey(provision);
    } catch (err) {
        console.log("CLI# ERROR rotateIAMUserAccessKeyB", err);
    }
}

async function checkIAMUserAccessKey(provision) {
    try {
        return await awscli.checkAccessKeys(provision);
    } catch (err) {
        console.log("CLI# ERROR checkIAMUserAccessKey", err);
    }
}

module.exports.deallocate = deallocate;
module.exports.start = start;
module.exports.updateVmsTags = updateVmsTags;
module.exports.stopDb = stopDb;
module.exports.startDb = startDb;
module.exports.createSnapshots = createSnapshots;
module.exports.rotateStorageAccountKey = rotateStorageAccountKey;
module.exports.rotateIAMUserAccessKey = rotateIAMUserAccessKey;
module.exports.checkIAMUserAccessKey = checkIAMUserAccessKey;