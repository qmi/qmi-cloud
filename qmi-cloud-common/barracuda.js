const BarracudaAPI = require('barracuda-api');
const azurecli = require('./azurecli');
const db = require('./mongo');

const DOMAIN = "qmi.qlik-poc.com";

async function createApp(prov) {

    if ( !prov._scenarioDoc.barracudaTemplate || prov._scenarioDoc.barracudaTemplate === 'qs-only-443' ) {
        _createApp(prov, "443", "HTTPS");
    } else if ( prov._scenarioDoc.barracudaTemplate === 'qdc-only-8443' ) {
        _createApp(prov, "8443", "HTTPS");
    } else if (prov._scenarioDoc.barracudaTemplate) {
        var spl = prov._scenarioDoc.barracudaTemplate.split("-");
        _createApp(prov, spl[0], spl[1]);
    }
}

async function _createApp(provision, backendPort, backendType) {

    
    if ( !provision || !provision.barracudaAzureFqdn ) {
        console.log(`Barracuda# Provision does not exist or does not have a barracudaAzureFqdn value. Do nothing.`);
        return;
    } 

    let appName = provision.scenario.toLowerCase();
    appName = appName.replace(/azqmi/g, 'qmi');
    appName = `${appName}-${provision._id}`

    console.log(`Barracuda# Creating Barracuda App for provision (${appName})`);

    const client = new BarracudaAPI.QlikSenseNPrintingApp();
    client.create(appName, `${appName}.${DOMAIN}`, provision.barracudaAzureFqdn, backendPort, backendType).then(function(result) {
        let bApp = result.app;
        console.log("Barracuda# Barracuda App created! ID: "+ bApp.id);
        let cname = bApp.waas_services && bApp.waas_services.length? bApp.waas_services[0].cname : null;
        db.provision.update(provision._id, {"barracudaAppId": bApp.id, "barracudaAppCname":cname});

        console.log("Barracuda# Creating DNS record in Azure: "+cname);
        azurecli.createDNSRecord(provision, cname);

        db.event.add({ provision: provision._id, type: 'provision.is-public' });

    }).catch(function(err){
        console.log("Barracuda# Error creating Barracuda App", err);
    });
}

async function deleteApp(provision) {

    
    if ( !provision || !provision.barracudaAppId ) {
        console.log(`Barracuda# Provision does not exist or does not have a barracudaAppId value. Do nothing.`);
        return;
    } 

    console.log(`Barracuda# Deleting DNS record in Azure and Barracuda App ID (${provision.barracudaAppId}) for provision (${provision._id})`);

    azurecli.deleteDNSRecord(provision);    
    const client = new BarracudaAPI.QlikSenseNPrintingApp();
    client.delete(provision.barracudaAppId).then(function() {
        console.log("Barracuda# Barracuda App deleted! ID: "+ provision.barracudaAppId);

        db.event.add({ provision: provision._id, type: 'provision.is-private' });
    }).catch(function(err){
        console.log("Barracuda# Error deleting Barracuda App", err);
    });
    
}

async function getApp(provision) {

    if ( !provision || !provision.barracudaAppId ) {
        console.log(`Barracuda# Provision does not exist or does not have a barracudaAppId value. Do nothing.`);
        return;
    } 
    const client = new BarracudaAPI.QlikSenseNPrintingApp();
    return await client.getDetails(provision.barracudaAppId);
    
}

module.exports.getApp = getApp;
module.exports.createApp = createApp;
module.exports.deleteApp = deleteApp;