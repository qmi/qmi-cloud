echo "--- Building QMI Cloud docker images for branch $CI_COMMIT_REF_NAME"

IMAGE_NAME="qlikgear/qmi-cloud-app"
PROJECT_FOLDER="."

export VERSION=$(cat $PROJECT_FOLDER/package.json | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[",]//g' | tr -d '[[:space:]]')

export TAG=$VERSION
export STABLE_TAG="latest"
export BUILD_ENV=""

if [ "$CI_COMMIT_REF_NAME" != "master" ]; then 
    TAG="$VERSION-$CI_COMMIT_REF_NAME"
    STABLE_TAG="latestdev"
    BUILD_ENV="staging"
fi

echo "--- Building image: $IMAGE_NAME:$TAG"
docker build -f $PROJECT_FOLDER/Dockerfile --build-arg BUILD_ENV=$BUILD_ENV  -t $IMAGE_NAME:$TAG ./
echo "--- Pushing image: $IMAGE_NAME:$TAG"
docker push $IMAGE_NAME:$TAG
docker image tag $IMAGE_NAME:$TAG $IMAGE_NAME:$STABLE_TAG
docker push $IMAGE_NAME:$STABLE_TAG  
