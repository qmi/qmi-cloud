const axios = require('axios');
const fs = require('fs');
const path = require('path');
const config = require('./config');
const qs = require('qs');
const OKTA_GROUP_SUPPORT = process.env.OKTA_GROUP_SUPPORT || "support";
const OKTA_GROUP_PRESALES = process.env.OKTA_GROUP_SUPPORT || "sales";
const OKTA_GROUP_SERVICES = process.env.OKTA_GROUP_SUPPORT || "services";


const getUserMsGraph = async function(trigramEmail, doSavePhoto) {
    console.log("Passport# Getting MsGraph data for user ", trigramEmail);
    try {
        var msAccessToken;
        const msConfig = {
            client_id: config.creds.azureAdClientId,
            client_secret: config.creds.azureAdClientSecret,
            scope: "https://graph.microsoft.com/.default",
            grant_type: "client_credentials"

        }
        const msTokenRes = await axios.post('https://login.microsoftonline.com/c21eeb5f-f5a6-44e8-a997-124f2f7a497c/oauth2/v2.0/token', qs.stringify(msConfig), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });

        msAccessToken = msTokenRes.data.access_token;

        var msUser = await axios({
            method: "GET",
            headers: {
                Authorization: "Bearer "+ msAccessToken
            },
            url: `https://graph.microsoft.com/v1.0/users/${trigramEmail}`
        });

        if (doSavePhoto){
            //Save user photo
            saveUserPhoto(trigramEmail, msUser.data.id, msAccessToken);
        }
        return {
            user: msUser.data,
            msAccessToken: msAccessToken
        };

    } catch (e){
        console.log('Passport# Error MS Graph stuff!!');
        return {
            data: null,
            msAccessToken: msAccessToken
        };
    }
}


const getUserGroups = async function(oid, msAccessToken) {
        var groups = [];
        return axios({
            method: 'GET',
            url: `https://graph.microsoft.com/v1.0/users/${oid}/memberOf`,
            //url: 'https://graph.microsoft.com/v1.0/groups',
            params: {
                "$top": 500,
                "$select": "displayName,id",
                "$count": true
            },
            headers: { 'Authorization' : 'Bearer '+msAccessToken }
        }).then(function (response) {
            console.log(`Passport# Groups found for user (${oid})`);
            if ( response.data  && response.data.value ) {
                response.data.value.forEach(d => {
                    if ( d.displayName && d.displayName !== "" ) {
                        let name = d.displayName.toLowerCase();
                        if ( name.includes("sg-") || name.includes("dl-") ) {
                            groups.push(d.displayName);
                        }
                    }   
                });
            }

            return groups;
            
        }).catch(function(err){
            console.log(`Passport# Error: querying groups for user (${oid})`);
            return groups;
        });
    
}

const saveUserPhoto = async function(trigramEmail, oid, msAccessToken) {
    const pic_path = path.resolve(__dirname, '..', 'photos', `${oid}.jpg`);
    if (!fs.existsSync(pic_path)) {

        if (!msAccessToken) {
            const msConfig = {
                client_id: config.creds.azureAdClientId,
                client_secret: config.creds.azureAdClientSecret,
                scope: "https://graph.microsoft.com/.default",
                grant_type: "client_credentials"

            }
            const msTokenRes = await axios.post('https://login.microsoftonline.com/c21eeb5f-f5a6-44e8-a997-124f2f7a497c/oauth2/v2.0/token', qs.stringify(msConfig), {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });

            msAccessToken = msTokenRes.data.access_token;
        }

        //Save user photo
    
        axios({
            method: 'GET',
            url: `https://graph.microsoft.com/v1.0/users/${trigramEmail}/photo/$value`,
            responseType: 'stream',
            headers: { 'Authorization' : 'Bearer '+msAccessToken }
        }).then(function (response) {
            
            console.log(`Passport# Picture found for user (${trigramEmail}) - Saving it to path: ${pic_path}`);
            response.data.pipe(fs.createWriteStream(pic_path));
        }).catch(function(err){
            console.log(`Passport# Error: No picture found for user (${trigramEmail}) or other error- Do nothing`);
        });
    } else {
        console.log(`Passport# Picture already exists for this user (${trigramEmail}) - Do nothing.`);
    }
}

module.exports.getUserMsGraph = getUserMsGraph;
module.exports.saveUserPhoto = saveUserPhoto;
module.exports.getUserGroups = getUserGroups;