const express = require('express');
const router = express.Router();
const db = require('@QMI/qmi-cloud-common/mongo');
const passport = require('../passport-okta');


/**
 * @swagger
 * /apikeys:
 *    get:
 *      description: Get all API keys
 *      summary: Get all API keys
 *      tags:
 *        - admin
 *      parameters:
 *        - name: filter
 *          in: query
 *          required: false
 *          type: object
 *          content:
 *            application/json: 
 *              schema:
 *                type: object
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: API KEYS
 */
router.get('/', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
      const filter = req.query.filter? JSON.parse(req.query.filter) : {};
      const result = await db.apiKey.get(filter);
      return res.json(result);
    } catch (error) {
      next(error);
    }
});

/**
 * @swagger
 * /apikeys/{userId}:
 *    post:
 *      description: Create an API for the userId
 *      summary: Create an API for the userId
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: userId
 *          in: path
 *          type: string
 *          required: true
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                description:
 *                  type: string
 *      responses:
 *        200:
 *          description: API KEY
 */
router.post('/:userId', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
      const user = await db.user.getById(req.params.userId);
      if ( !user ) {
          res.status(404).json({"err": "user not found"});
      }
      var body = req.body;
      body.user = req.params.userId;
      const result = await db.apiKey.add(body);
      return res.json(result);
    } catch (error) {
      next(error);
    }
});

/**
 * @swagger
 * /apikeys/{id}/revoke:
 *    put:
 *      description: Revoke API Key
 *      summary: Revoke API Key
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      responses:
 *        200:
 *          description: API KEY
 */
router.put('/:id/revoke', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
        const result = await db.apiKey.update(req.params.id, {"isActive": false});
        return res.json(result);
    } catch (error) {
      next(error);
    }
});

/**
 * @swagger
 * /apikeys/{id}:
 *    delete:
 *      description: Delete API Key
 *      summary: Delete API Key
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      responses:
 *        200:
 *          description: API KEY
 */
 router.delete('/:id', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    const result = await db.apiKey.del(req.params.id);
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;