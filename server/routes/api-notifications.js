const express = require('express');
const router = express.Router();
const db = require('@QMI/qmi-cloud-common/mongo');
const passport = require('../passport-okta');
const sendEmail = require('@QMI/qmi-cloud-common/send-email');


/**
 * @swagger
 * /notifications:
 *    get:
 *      description: Get all notifications
 *      summary: Get all notifications
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: page
 *          in: query
 *          required: false
 *          type: integer
 *        - name: size
 *          in: query
 *          required: false
 *          type: integer
 *      responses:
 *        200:
 *          description: Notifications
 */
router.get('/', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
      var page;
      if ( req.query.page && req.query.size ) {
        page = {
          page: parseInt(req.query.page), 
          size: parseInt(req.query.size)
        }
      }
      const result = await db.notification.getPage({}, page);
      return res.json(result);
    } catch (error) {
      next(error);
    }
  });

/**
 * @swagger
 * /notifications/testsendemail:
 *    post:
 *      description: Test email service
 *      summary: Test email service
 *      tags:
 *        - admin
 *      parameters:
 *        - name: to
 *          in: query
 *          required: false
 *          type: string
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Notifications
 */
router.post('/testsendemail', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try { 
    const result = await sendEmail._doSend(req.query.to || req.user.upn, "QMI - Test email", "Hi! This is a test email from QMI Cloud.");
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;