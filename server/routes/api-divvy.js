const express = require('express');
const router = express.Router();
const divvy = require('@QMI/qmi-cloud-common/divvy');
const passport = require('../passport-okta');


/**
 * @swagger
 * /divvy/events:
 *    post:
 *      description: DivvyCloud webhook
 *      tags:
 *        - admin
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Notifications
 */
router.post('/events', passport.ensureAuthenticatedAndAdmin, async (req, res) => {

    divvy.onEvent(req.body, req.user);
    return res.json(req.body);

});

module.exports = router;