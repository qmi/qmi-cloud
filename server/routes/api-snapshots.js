const express = require('express');
const router = express.Router();
const db = require('@QMI/qmi-cloud-common/mongo');
const passport = require('../passport-okta');

const MYQUEUES = require('@QMI/qmi-cloud-common/queues');
const queues = MYQUEUES.queues;
const SYNAPSE_QUEUE = MYQUEUES.SYNAPSE_QUEUE;


/**
 * @swagger
 * /snapshots:
 *    get:
 *      description: Get all snaphots
 *      summary: Get all snaphots
 *      tags:
 *        - admin
 *      parameters:
 *        - name: filter
 *          in: query
 *          required: false
 *          type: object
 *          content:
 *            application/json: 
 *              schema:
 *                type: object
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: User
 */
router.get('/', passport.ensureAuthenticated, async (req, res, next) => {
  try {
    const filter = req.query.filter? JSON.parse(req.query.filter) : {};
    const result = await db.snapshot.get(filter, null, null, null, '["provision", "owner"]');
    return res.json(result);
  } catch (error) {
    next(error);
  }
});


/**
 * @swagger
 * /snapshots/{id}:
 *    get:
 *      description: Get snapshot details
 *      summary: Get snapshot details
 *      tags:
 *        - admin
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: User
 */
router.get('/:id', passport.ensureAuthenticated, async (req, res, next) => {
  try {
    const result = await db.snapshot.getById(req.params.id);
    if (!result){
      return res.status(404).json({"msg": "Not found"});
    }
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * /snapshots/{id}:
 *    put:
 *      description: Update a Snapshot
 *      summary: Update a Snapshot
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *        - in: body
 *          name: body
 *          description: Snapshot object
 *          required: true
 *      responses:
 *        200:
 *          description: WEBHOOK
 */
router.put('/:id', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    var patch = req.body;
    patch.owner = req.user._id;
    const result = await db.snapshot.update(req.params.id, patch);
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * /snapshots/{id}/copy:
 *    post:
 *      description: Copy snapshots into regions
 *      summary: Copy snapshots into regions
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *        - name: regions
 *          in: query
 *          required: false
 *          type: string
 *      responses:
 *        200:
 *          description: OK
 *        404:
 *          description: Not found
 *        
 */
 router.post('/:id/copy', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {

    const snap = await db.snapshot.getById(req.params.id);
    if (!snap){
      return res.status(404).json({"msg": "Not found"});
    }

    if (snap.status === "copy.init" ) {
      return res.json({"msg": "Snapshots already being copyed", "success": true});
    }
    let regions = req.query.regions? req.query.regions : '"europe asia eastus"';

    queues[SYNAPSE_QUEUE].add("synapse_job", {
      snapId: snap._id,
      provision: snap.provision,
      snapName: snap.name,
      regions: regions,
      tasktype: "copy-snapshots"
    });

    await db.snapshot.update(req.params.id, {"status": "copy.init"});

    return res.json({"msg": "Snapshots are being copyed into "+regions, "success": true});

  } catch (error) {
    next(error);
  }
});


/**
 * @swagger
 * /snapshots/{id}/copystatus:
 *    get:
 *      description: Get Status of copy snapshots into regions
 *      summary: Get Status of copy snapshots into regions
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      responses:
 *        200:
 *          description: OK
 *        404:
 *          description: Not found
 *        
 */
router.get('/:id/copystatus', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {

    const snap = await db.snapshot.getById(req.params.id);
    if (!snap){
      return res.status(404).json({"msg": "Not found"});
    }

    queues[SYNAPSE_QUEUE].add("synapse_job", {
      snapId: snap._id,
      provision: snap.provision,
      snapName: snap.name,
      tasktype: "check-copy-snapshots"
    });
  
    return res.json({"msg": "Snapshots are being checked", "success": true});

  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * /snapshots/{id}/logs:
 *    get:
 *      description: Get logs for snapshots
 *      summary: Get logs for snapshots
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      responses:
 *        200:
 *          description: OK
 *        404:
 *          description: Not found
 *        
 */
router.get('/:id/logs', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    
    const snap = await db.snapshot.getById(req.params.id);
    if (!snap){
      return res.status(404).json({"msg": "Not found"});
    }
    return res.sendFile(`/logs/snapshot/${snap._id}.log`);
  } catch (error) {
    next(error);
  }
});




module.exports = router;