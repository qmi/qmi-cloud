const express = require('express');
const router = express.Router();
const db = require('@QMI/qmi-cloud-common/mongo');
const passport = require('../passport-okta');


/**
 * @swagger
 * /webhooks:
 *    get:
 *      description: Get all Webhooks
 *      summary: Get all Webhooks
 *      tags:
 *        - admin
 *      parameters:
 *        - name: filter
 *          in: query
 *          required: false
 *          type: object
 *          content:
 *            application/json: 
 *              schema:
 *                type: object
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: WEBHOOKS
 */
router.get('/', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
      const filter = req.query.filter? JSON.parse(req.query.filter) : {};
      const result = await db.webhook.get(filter);
      return res.json(result);
    } catch (error) {
      next(error);
    }
});

/**
 * @swagger
 * /webhooks:
 *    post:
 *      description: Create a Webhook
 *      summary: Create a Webhook
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *      responses:
 *        200:
 *          description: WEBHOOK
 */
router.post('/', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    var body = req.body;
    body.owner = req.user._id;
    var result;
    console.log("BODY", body);
    if (  body._id ) {
      result = await db.webhook.update( body._id, body);
    } else {
      result = await db.webhook.add(body);
    }
    
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * /webhooks/{id}:
 *    delete:
 *      description: Delete Webhook
 *      summary: Delete Webhook
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      responses:
 *        200:
 *          description: WEBHOOK
 */
router.delete('/:id', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    const result = await db.webhook.del(req.params.id);
    return res.json(result);
  } catch (error) {
    next(error);
  }
});


module.exports = router;