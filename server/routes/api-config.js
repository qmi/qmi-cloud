const express = require('express')
const router = express.Router()

const qmiConfig = require('@QMI/qmi-cloud-common/qmi-config').qmiConfig;

router.get('/', async (req, res, next) => {
    try {
      return res.json(qmiConfig());
    } catch (error) {
      next(error);
    }   
});

module.exports = router;