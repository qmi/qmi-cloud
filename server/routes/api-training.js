const express = require('express');
const router = express.Router();
const db = require('@QMI/qmi-cloud-common/mongo');
const passport = require('../passport-okta');
const cloudshare = require('../training/cloudshare');
const qa = require('../training/automations');


/**
 * @swagger
 * /training/sessions:
 *    get:
 *      description: Get all training sessions (admin)
 *      summary:  Get all training sessions (admin)
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: TrainingTemplate
 */
router.get('/sessions', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    const filter = req.query.filter? JSON.parse(req.query.filter) : {};
    const result = await db.trainingSession.get(filter);
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * /training/templates:
 *    get:
 *      description: Get all training templates
 *      summary: Get all training templates
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: TrainingTemplate
 */
router.get('/templates', passport.ensureAuthenticated, async (req, res, next) => {
    try {
      const filter = req.query.filter? JSON.parse(req.query.filter) : {};
      const result = await db.trainingTemplate.get(filter);
      return res.json(result);
    } catch (error) {
      next(error);
    }
});

/**
 * @swagger
 * /training/session/{id}:
 *    get:
 *      description: Get training session by ID
 *      summary: Get training session by ID
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: id 
 *          in: path
 *          type: string
 *          required: true
 *      responses:
 *        200:
 *          description: TrainingTemplate
 */
router.get('/session/:id', async (req, res, next) => {
  try {
    const result = await db.trainingSession.get({"_id":req.params.id}, 'passwd -user description status created updated studentEmailFilter');
    if (!result || !result.results || result.results.length === 0 ){
      return res.status(404).json({"msg": "Not found"});
    }
    return res.json(result.results[0]);
  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * /training/session/{id}:
 *    post:
 *      description: Post new student for session by ID
 *      summary: Post new student for session by ID
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: id 
 *          in: path
 *          type: string
 *          required: true
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *      responses:
 *        200:
 *          description: TrainingTemplate
 */
router.post('/session/:id', async (req, res, next) => {
  try {
    const session = await db.trainingSession.getById(req.params.id);
    if (!session){
      return res.status(404).json({"msg": "Not found"});
    }

    let student = await db.trainingStudent.getOne({"email": req.body.email, "session": session._id});
    let isNew = false;
    if ( !student ) {
      let data = req.body;
      data.session = session._id;
      student = await db.trainingStudent.add(data);
      isNew = true;
    } 

    let result1, result2;
    if ( session.template.cloudshare && session.cloudshareClass ) {
      result1 = await cloudshare.addStudentToClass(session, student.email);
    } 
    if ( session.template.needQcsAutomation && session.qaUrl && session.qaToken ) {
      result2 = await qa.runQlikAutomation(session, student.email);
    }
    
    if (result1 && result1.error || result2 && result2.error) {
      return res.status(500).json({"msg": "Something went wrong when registering studeent", err: result1.error? result1.error : result2.error});
    } else {
      student = await db.trainingStudent.update(student._id, {"status": "sent", "created": new Date()});
      if ( isNew ) {
        db.trainingSession.update({ _id: session._id }, { $inc: { studentsCount: 1 } });
      }
      return res.json(student);
    }  
    
  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * /training/session/{id}:
 *    delete:
 *      description: Delete session by ID
 *      summary: Delete session by ID
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: id 
 *          in: path
 *          type: string
 *          required: true
 *      responses:
 *        200:
 *          description: TrainingTemplate
 */
router.delete('/session/:id', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    const result = await db.trainingSession.update(req.params.id,  {status: "terminated"});
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * /training/templates:
 *    post:
 *      description: Create new training template
 *      summary: Create new training template
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                description:
 *                  type: string
 *      responses:
 *        200:
 *          description: TrainingTemplate
 */
router.post('/templates', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
      let data = req.body;
      const result = await db.trainingTemplate.add(data);
      return res.json(result);
    } catch (error) {
      next(error);
    }
});


/**
 * @swagger
 * /training/{userId}/sessions:
 *    post:
 *      description: Add new training session
 *      summary: Add new training session
 *      parameters:
 *        - name: userId
 *          in: path
 *          type: string
 *          required: true
*      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: TrainingSession
 */
router.post('/:userId/sessions', passport.ensureAuthenticatedAndIsMe, async (req, res, next) => {
    try {
      const userId = req.params.userId === 'me'? req.user._id : req.params.userId;
      let data = req.body;
      data.user = userId;
      let session = await db.trainingSession.add(data);
      session = await db.trainingSession.getById(session._id);

      let mainAutomation = await qa.getAutomation(session, 'main');
      if (!mainAutomation) {
         let result = await qa.createAutomations(session);
         mainAutomation = result.main;
      }
      session = await db.trainingSession.update(session._id, {qaToken: mainAutomation.executionToken, qaUrl: `https://${session.qcsTenantHost}/api/v1/automations/${mainAutomation.id}/actions/execute`})

      return res.json(session);
    } catch (error) {
      next(error);
    }
  });

/**
 * @swagger
 * /training/{userId}/sessions:
 *    get:
 *      description: Get all training session by user
 *      summary: Get all training session by user
 *      parameters:
 *        - name: userId
 *          in: path
 *          type: string
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: TrainingSession
 */
router.get('/:userId/sessions', passport.ensureAuthenticatedAndIsMe, async (req, res, next) => {
    try {
      const userId = req.params.userId === 'me'? req.user._id : req.params.userId;
      const result = await db.trainingSession.get({user: userId, status: {"$ne": "terminated"}});
      return res.json(result);
    } catch (error) {
      next(error);
    }
  });

/**
 * @swagger
 * /training/{userId}/sessions/{id}:
 *    get:
 *      description: Get training session by Id for a user
 *      summary: Get training session by Id for a user
 *      parameters:
 *        - name: userId
 *          in: path
 *          type: string
 *          required: true
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: TrainingSession
 */
router.get('/:userId/sessions/:id', passport.ensureAuthenticatedAndIsMe, async (req, res, next) => {
    try {
      const userId = req.params.userId === 'me'? req.user._id : req.params.userId;
      const id = req.params.id;
      const result = await db.trainingSession.getOne({user: userId, _id: id});
      return res.json(result);
    } catch (error) {
      next(error);
    }
  });

/**
 * @swagger
 * /training/{userId}/sessions/{id}/automation:
 *    get:
 *      description: Test if automation exists in tenant
 *      summary: Test if automation exists in tenant
 *      parameters:
 *        - name: userId
 *          in: path
 *          type: string
 *          required: true
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: TrainingSession
 */
router.get('/:userId/sessions/:id/automation', passport.ensureAuthenticatedAndIsMe, async (req, res, next) => {
  try {
    const id = req.params.id;
    let session = await db.trainingSession.getById(id);

    let automation = await qa.getAutomation(session);
    if (automation) {
        session = await db.trainingSession.update(session._id, {qaToken: automation.executionToken, qaUrl: `https://${session.qcsTenantHost}/api/v1/automations/${automation.id}/actions/execute`})
    }
    return res.json(session);
  } catch (error) {
    next(error);
  }
});

  /**
 * @swagger
 * /training/session/{id}/students:
 *    get:
 *      description: Get studetns for a session
 *      summary: Get studetns for a session
 *      parameters:
 *        - name: userId
 *          in: path
 *          type: string
 *          required: true
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: TrainingSession
 */
router.get('/session/:id/students', passport.ensureAuthenticated, async (req, res, next) => {
  try {
    const id = req.params.id;
    const result = await db.trainingStudent.get({session: id});
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * /training/{userId}/sessions/{id}:
 *    put:
 *      description: Update training session
 *      summary: Update training session
 *      parameters:
 *        - name: userId
 *          in: path
 *          type: string
 *          required: true
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: TrainingSession
 */
router.put('/:userId/sessions/:id', passport.ensureAuthenticatedAndIsMe, async (req, res, next) => {

  try {
    const result = await db.trainingSession.update(req.params.id, req.body);
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * /training/{userId}/sessions/{id}:
 *    put:
 *      description: Update training session
 *      summary: Update training session
 *      parameters:
 *        - name: userId
 *          in: path
 *          type: string
 *          required: true
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: TrainingSession
 */
router.delete('/:userId/sessions/:id', passport.ensureAuthenticatedAndIsMe, async (req, res, next) => {

  try {
    const result = await db.trainingSession.update(req.params.id, {status: "terminated"});
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;