const axios = require("axios");
const AUTOMATION_NAME_PREFIX = "QMI-Training";

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
  }
}

const _newAutomation = async function(host, apikey, index, workspace) {
    let workspaceData = require(`./workspaces/${index}-${workspace}.json`);
    let name = `${AUTOMATION_NAME_PREFIX}-${index}-${workspace}`;

    let result = await axios({
      method: "post",
      url: `https://${host}/api/v1/automations/`,
      headers: {
        "Authorization": `Bearer ${apikey}`,
        "Content-Type": "application/json",
        "Accept": "application/json"
      },
      data: {
        "id":"create",
        "name": name,
        "description": workspace === 'main'? "Main automation entry point for QMI training session" : ""
      },
    });
    
    let data = result.data;
    console.log("Trainning# createAutomation #step1", data);

    let result2 = await axios({
      method: "put",
      url: `https://${host}/api/v1/automations/${data.id}`,
      headers: {
        "Authorization": `Bearer ${apikey}`,
        "Content-Type": "application/json",
        "Accept": "application/json"
      },
      data: {
        "workspace": workspaceData
      },
    });

    console.log("Trainning# createAutomation #step2", result2.data);
    return result2.data;

 
};

async function runQlikAutomation(session, email) {
  
  let automationUrl = session.qaUrl;
  let automationToken = session.qaToken;
  
  try {
    var result = await axios({
      method: "post",
      url: automationUrl,
      headers: {
        "X-Execution-Token": automationToken,
        "Content-Type": "application/json"
      },
      data: {
        email: email,
        apiKey: session.qcsApiKey,
        sharedSpace: session.qcsSharedSpace? session.qcsSharedSpace : null,
        dataSpace: session.qcsDataSpace? session.qcsDataSpace :  null
      },
    });
    
    console.log(`Trainning# Executed Qlik Automation '${automationUrl}' for user '${email}'`);

    return result.data;
  } catch (error) {
    return {error: error};
  }
}

async function getAutomation(session, workspace) {
  const name = `${AUTOMATION_NAME_PREFIX}-${session.template.index}-${workspace}`;
  console.log(`Trainning# GetAutomation: find automation with name (${name}) in tenant (${session.qcsTenantHost})`);
  try {
    var result = await axios({
      method: "get",
      url: `https://${session.qcsTenantHost}/api/v1/automations?filter=name eq "${name}"`,
      headers: {
        "Authorization": `Bearer ${session.qcsApiKey}`,
        "Content-Type": "application/json",
        "Accept": "application/json"
      }
    });
    
    if ( result.data && result.data.length ) {
      console.log("Trainning# GetAutomation found!!", result.data[0]);
      return result.data[0];
    } else {
      console.log("Trainning# GetAutomation", 'Not found!!');
      return null;
    }


  } catch (error) {
    return {error: error};
  }
}

async function createAutomations(session) {

  try {
    console.log("Trainning# CreateAutomation for session ", session._id);
    let result = {};
    console.log('Trainning# CreateAutomation asyncForEach', session.template.needQcsAutomation);
    await asyncForEach(session.template.needQcsAutomation, async function( workspace )  {
      console.log("Trainning# CreateAutomation EXECUTING create new automation: ", workspace);
      result[workspace] = await _newAutomation(session.qcsTenantHost, session.qcsApiKey, session.template.index, workspace);
    });
    console.log("Trainning# Final results create automations", result);
    return result;

  } catch (error) {
    return {error: error};
  }
}

module.exports.runQlikAutomation = runQlikAutomation;
module.exports.createAutomations = createAutomations;
module.exports.getAutomation = getAutomation;
