
const secrets = require("/run/secrets/config.json");

const HOSTNAME_URL = process.env.HOSTNAME_URL || "http://localhost:3000"
const CLIENT_ID = secrets.CLIENT_ID;
const CLIENT_SECRET = secrets.CLIENT_SECRET;

const AZURE_AD_CLIENT_ID = secrets.AZURE_AD_CLIENT_ID;
const AZURE_AD_CLIENT_SECRET = secrets.AZURE_AD_CLIENT_SECRET;

const LOGOUT_URL = HOSTNAME_URL;
const REDIRECT_URL = `${HOSTNAME_URL}/auth/openid/return`;

exports.creds = {
    // Required
    //identityMetadata: IDENTITY_METADATA,
    //
    // or you can use the common endpoint
    // 'https://login.microsoftonline.com/common/.well-known/openid-configuration'
    // To use the common endpoint, you have to either set `validateIssuer` to false, or provide the `issuer` value.
  
    // Required, the client ID of your app in AAD
    clientID: CLIENT_ID,
  
    // Required, must be 'code', 'code id_token', 'id_token code' or 'id_token'
    responseType: 'id_token code',
  
    // Required
    responseMode: 'form_post',
  
    // Required, the reply URL registered in AAD for your app
    redirectUrl: REDIRECT_URL,
  
    // Required if we use http for redirectUrl
    allowHttpForRedirectUrl: true,
  
    // Required if `responseType` is 'code', 'id_token code' or 'code id_token'.
    // If app key contains '\', replace it with '\\'.
    clientSecret: CLIENT_SECRET,
  
    // Required to set to false if you don't want to validate issuer
    validateIssuer: true,
  
    // Required to set to true if you are using B2C endpoint
    // This sample is for v1 endpoint only, so we set it to false
    isB2C: false,
  
    // Required if you want to provide the issuer(s) you want to validate instead of using the issuer from metadata
    issuer: null,
  
    // Required to set to true if the `verify` function has 'req' as the first parameter
    passReqToCallback: false,
  
    // Recommended to set to true. By default we save state in express session, if this option is set to true, then
    // we encrypt state and save it in cookie instead. This option together with { session: false } allows your app
    // to be completely express session free.
    useCookieInsteadOfSession: true,
  
    // Required if `useCookieInsteadOfSession` is set to true. You can provide multiple set of key/iv pairs for key
    // rollover purpose. We always use the first set of key/iv pair to encrypt cookie, but we will try every set of
    // key/iv pair to decrypt cookie. Key can be any string of length 32, and iv can be any string of length 12.
    cookieEncryptionKeys: [
      { 'key': '12345678901234567890123456789012', 'iv': '123456789012' },
      { 'key': 'abcdefghijklmnopqrstuvwxyzabcdef', 'iv': 'abcdefghijkl' }
    ],
  
    // Optional. The additional scope you want besides 'openid', for example: ['email', 'profile'].
    scope: ['openid', 'email', 'profile'],
  
    // Optional, 'error', 'warn' or 'info'
    loggingLevel: 'warn',
  
    // Optional. The lifetime of nonce in session or cookie, the default value is 3600 (seconds).
    nonceLifetime: null,
  
    // Optional. The max amount of nonce saved in session or cookie, the default value is 10.
    nonceMaxAmount: 5,
  
    // Optional. The clock skew allowed in token validation, the default value is 300 seconds.
    clockSkew: null,

    azureAdClientId: AZURE_AD_CLIENT_ID,
    azureAdClientSecret: AZURE_AD_CLIENT_SECRET
};

// Optional.
// If you want to get access_token for a specific resource, you can provide the resource here; otherwise,
// set the value to null.
// Note that in order to get access_token, the responseType must be 'code', 'code id_token' or 'id_token code'.
exports.resourceURL = 'https://graph.microsoft.com';

// The url you need to go to destroy the session with AAD
//exports.destroySessionUrl = `https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=${LOGOUT_URL}`;
//exports.destroySessionUrl = `https://qlik.okta.com/logout?id_token_hint=${id_token}&post_logout_redirect_uri=${LOGOUT_URL}&state=${state}`
exports.destroySessionUrl = `https://qlik.okta.com/logout?post_logout_redirect_uri=${LOGOUT_URL}`
// If you want to use the mongoDB session store for session middleware, set to true; otherwise we will use the default
// session store provided by express-session.
// Note that the default session store is designed for development purpose only.
exports.useMongoDBSessionStore = true;

// If you want to use mongoDB, provide the uri here for the database.
exports.databaseUri = process.env.MONGO_URI;

// How long you want to keep session in mongoDB.
exports.mongoDBSessionMaxAge = 12 * 60 * 60;  // 0.5 day (unit is second)