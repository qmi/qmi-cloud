# Stage 1: 
FROM node:20.14-alpine AS sources
  
WORKDIR /app

ADD ./package.json ./
ADD ./.npmrc ./

RUN yarn install --production

# Stage 2: 
FROM node:20.14-alpine AS production

ARG BUILD_ENV

RUN echo "Building enviroment is: $BUILD_ENV"

WORKDIR /app
COPY --from=sources /app/node_modules ./node_modules
COPY --from=sources /app/package.json ./package.json
COPY ./server ./server
COPY ./config ./config
COPY ./dist${BUILD_ENV} ./dist
COPY ./mystatsmashup ./mystatsmashup

EXPOSE 3000
EXPOSE 3100

CMD ["node", "server/server"]
